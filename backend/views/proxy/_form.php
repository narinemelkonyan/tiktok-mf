<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Proxy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="Proxy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput(['autocomplete' => 'off']) ?>

    <?= $form->field($model, 'password')->textInput(['autocomplete' => 'off']) ?>

    <?= $form->field($model, 'IP')->textInput(['autocomplete' => 'off']) ?>

    <?= $form->field($model, 'port')->textInput(['autocomplete' => 'off']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
