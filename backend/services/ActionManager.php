<?php

namespace backend\services;


use common\models\Task;
use common\models\TiktokAccount;
use yii\httpclient\Client;

class ActionManager
{
    const ACCOUNT_URL = '/accounts/';
    const ACCOUNT_CHECK_URL = '/accounts/is_active/';
    const SUBSCRIBE_URL = '/actions/sub/';
    const LIKE_URL = '/actions/like/';
    const VIEW_URL = '/actions/view/';
    const CHECK_URL = '/check/';

    const BOT_STATUS_COMPLETED = 1;

    public static function send($data, $url, $method = 'POST')
    {
        $client = new Client();

        $response = $client->createRequest()
            ->setMethod($method)
            ->setUrl(\Yii::$app->params['apiUrl'] . $url)
            ->setData("$user:$pass")
            ->send();

        try {
            $responseData = $response->getData();
            return $responseData;
        } catch (\Exception $e) {
            \Yii::error('The requested data was not received.','ActionManager.send');
        }

    }

    public static function addUuid($model, $uuid)
    {
        if ($model && $uuid) {
            $model->uuid = $uuid;
            $model->save();
        }
        return $model;

    }

    public static function changeUserStatus($userId, $status)
    {
        if ($userId) {
            $tikTokAccount = TiktokAccount::findOne($userId);
            if ($tikTokAccount) {
                $tikTokAccount->tiktok_status = $status == TiktokAccount::BOT_STATUS_ACTIVE ? TiktokAccount::STATUS_ACTIVE : TiktokAccount::STATUS_INVALID;
                $tikTokAccount->save();
            }
        }
    }

    public static function changeLastAuth($userId)
    {
        if ($userId) {
            $tikTokAccount = TiktokAccount::findOne($userId);
            if ($tikTokAccount) {
                $tikTokAccount->last_login = time();
                $tikTokAccount->save();
            }
        }
    }

    public static function changeStatus($model, $status)
    {
        if ($model && $status == self::BOT_STATUS_COMPLETED) {
            $model->status = Task::STATUS_COMPLETED;
            $model->save();
        }

        return $model;

    }


}