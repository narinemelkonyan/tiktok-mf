<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\models\Proxy;

/* @var $this yii\web\View */
/* @var $model common\models\Proxy */

$this->title = $model->IP . ':' . $model->port;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proxys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="Proxy-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'login',
            'password',
            'IP',
            'port',
            'check_date',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->status ? Proxy::getStatus()[$model->status] : null;
                }
            ],
        ],
    ]) ?>

</div>
