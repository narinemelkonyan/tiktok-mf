<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->null(),
        ], $tableOptions);

        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('TIKTOKadminTIKTOK'),
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'created_at' => time(),

        ]);

        $this->createTable('{{%group}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%tiktok_account}}', [
            'id' => $this->primaryKey(),
            'nickname' => $this->string()->notNull()->unique(),
            'url' => $this->string(32)->null(),
            'secure_id' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'password' => $this->string()->notNull(),
            'device_id' => $this->string()->null(),
            'fp' => $this->string()->null(),
            'iid' => $this->string()->null(),
            'openud_id' => $this->string()->null(),
            'max_repost' => $this->integer()->null(),
            'max_like' => $this->integer()->null(),
            'max_follower' => $this->integer()->null(),
            'to_completed' => $this->integer()->null(),
            'from_completed' => $this->integer()->null(),
            'registered_data' => $this->integer()->null(),
            'last_login' => $this->integer()->null(),
            'tiktok_status' => $this->integer()->null(),
            'group_id' => $this->integer()->null(),
            'created_at' => $this->integer()->null(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-user-group-id',
            'tiktok_account',
            'group_id',
            'group',
            'id'
        );

        $this->createTable('{{%action}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->string()->notNull()->unique(),
            'tiktok_account_id' => $this->integer()->notNull(),
            'whom_following_nickname' => $this->string()->null(),
            'is_subscribe_follower' => $this->boolean()->defaultValue(false),
            'min_follower' => $this->integer()->null(),
            'max_follower' =>  $this->integer()->null(),
            'video_url' => $this->string()->null(),
            'text' => $this->string()->null(),
            'repeat_count' =>  $this->integer()->null(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-action-tiktok-account-id',
            'action',
            'tiktok_account_id',
            'tiktok_account',
            'id'
        );

        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'start_at' => $this->integer()->notNull(),
            'execute_at' => $this->integer()->notNull(),
            'completed_at' => $this->integer()->null(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk-task-account-id',
            'task',
            'action_id',
            'action',
            'id'
        );

        $this->createTable('{{%proxi}}', [
            'id' => $this->primaryKey(),
            'login' => $this->integer()->notNull(),
            'password' => $this->integer()->notNull(),
            'IP' => $this->integer()->notNull(),
            'port' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'error_text' => $this->text()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%group}}');
        $this->dropTable('{{%tiktok_account}}');
        $this->dropTable('{{%action}}');
        $this->dropTable('{{%task}}');
        $this->dropTable('{{%proxi}}');
    }
}
