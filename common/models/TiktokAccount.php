<?php

namespace common\models;

use backend\services\ActionManager;
use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "tiktok_account".
 *
 * @property int $id
 * @property string $nickname
 * @property string|null $url
 * @property string|null $uuid
 * @property string|null $secure_id
 * @property string|null $tiktok_id
 * @property string $email
 * @property string|null $device_id
 * @property string|null $fp
 * @property string|null $iid
 * @property string|null $openud_id
 * @property int|null $max_repost
 * @property int|null $max_like
 * @property int|null $max_follower
 * @property int|null $max_view
 * @property string|null $to_completed
 * @property string|null $from_completed
 * @property int|null $registered_data
 * @property int|null $last_login
 * @property int|null $tiktok_status
 * @property int|null $group_id
 * @property int|null $created_at
 * @property int|null $max_comment
 * @property string $password
 *
 * @property Action[] $actions
 * @property Group $group
 */
class TiktokAccount extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INVALID = 2;
    const STATUS_AWAITING = 3;

    const BOT_STATUS_ACTIVE = 1;

    public $follower;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tiktok_account}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'device_id', 'fp', 'iid', 'openud_id', 'url'], 'required'],
            [['max_repost', 'max_like', 'max_view', 'max_follower', 'registered_data', 'last_login', 'tiktok_status', 'group_id', 'created_at', 'max_comment'], 'default', 'value' => null],
            [['max_repost', 'max_like', 'max_view', 'max_follower', 'registered_data', 'last_login', 'tiktok_status', 'group_id', 'created_at', 'max_comment'], 'integer'],
            [['nickname', 'secure_id', 'email', 'device_id', 'fp', 'iid', 'openud_id', 'password', 'tiktok_id'], 'string', 'max' => 255],
            [['url', 'uuid'], 'string', 'max' => 255],
            [['to_completed', 'from_completed'], 'string', 'max' => 5],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['nickname', 'device_id', 'iid', 'openud_id',], 'unique'],
            [['url'], 'unique'],
            ['password', 'required', 'when' => function ($model) {
                return $model->isNewRecord;
            },
                'whenClient' => "function (attribute, value) {
                    return {$this->isNewRecord};
                }"
            ],
            [['nickname'], 'match', 'pattern' => '/^@[\w,\.,\-]*$/i'],
//            [['device_id'], 'match', 'pattern' => '/[^0-9]/'],
//            [['fp', 'iid','openud_id'], 'match', 'pattern' => '/^[A-Za-z0-9]+$/i'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::class, 'targetAttribute' => ['group_id' => 'id']],
            [['from_completed', 'to_completed'], 'date', 'format' => 'H:m'],
            [['from_completed', 'to_completed'], 'validateTimeField'],
            ['url', 'common\validators\AccountValidator'],
            [['max_like', 'max_follower', 'max_view', 'max_repost'], 'number', 'min' => 1],
            [['max_like', 'max_follower', 'max_view', 'max_repost'], 'number', 'max' => 300],

        ];
    }

    public function validateTimeField($attribute)
    {
        $to_completed = strtotime($this->to_completed);
        $from_completed = strtotime($this->from_completed);

        if ($from_completed > $to_completed || $from_completed === $to_completed) {
            $this->addError($attribute, $this->getAttributeLabel('Неверно выбрано время'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nickname' => Yii::t('app', 'Никнейм'),
            'url' => Yii::t('app', 'Ссылка на аккаунт'),
            'secure_id' => Yii::t('app', 'Secure ID'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Пароль'),
            'device_id' => Yii::t('app', 'Device ID'),
            'fp' => Yii::t('app', 'Fp'),
            'iid' => Yii::t('app', 'Iid'),
            'openud_id' => Yii::t('app', 'Openud ID'),
            'max_repost' => Yii::t('app', 'Количество репостов'),
            'max_like' => Yii::t('app', 'Количество лайков'),
            'max_view' => Yii::t('app', 'Количество просмотров'),
            'max_follower' => Yii::t('app', 'Количество подписок'),
            'max_comment' => Yii::t('app', 'Количество комментариев'),
            'to_completed' => Yii::t('app', 'До'),
            'from_completed' => Yii::t('app', 'С'),
            'registered_data' => Yii::t('app', 'Registered Data'),
            'last_login' => Yii::t('app', 'Последняя авторизация'),
            'tiktok_status' => Yii::t('app', 'Статус'),
            'group_id' => Yii::t('app', 'Group ID'),
            'created_at' => Yii::t('app', 'Дата создания'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActions()
    {
        return $this->hasMany(Action::class, ['tiktok_account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::class, ['id' => 'group_id']);
    }

    public static function getStatus(): array
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Aктивный'),
            self::STATUS_INVALID => Yii::t('app', 'Невалидный'),
            self::STATUS_AWAITING => Yii::t('app', 'Ожидает'),
        ];
    }

    public function beforeValidate()
    {
        if ($this->from_completed == '00:00' || $this->to_completed == '00:00' || $this->from_completed == null || $this->to_completed == null) {
            $this->from_completed = Setting::FROM_COMPLETED;
            $this->to_completed = Setting::TO_COMPLETED;
        }
        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = time();
        }
        if ($this->max_like == null) {
            $this->max_like = Setting::MAX_LIKE;
        }
        if ($this->max_follower == null) {
            $this->max_follower = Setting::MAX_SUBSCRIBE;
        }
        if ($this->max_view == null) {
            $this->max_view = Setting::MAX_VIEW;
        }


        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->addAccountOnTikTOk($this, ActionManager::ACCOUNT_URL, 'POST');
        }

    }

    public static function getAccounts()
    {
        return yii\helpers\ArrayHelper::map(self::find()->where(['tiktok_status' => self::STATUS_ACTIVE])->all(), 'id', 'nickname');
    }


    public function addAccountOnTikTOk($account, $url, $method)
    {
        if ($account) {
            $sentData = [
                "email" => $account->email,
                "pass" => $account->password,
                "device_id" => $account->device_id,
                "fp" => $account->fp,
                "iid" => $account->iid,
                "openudid" => $account->openud_id,
            ];
            if ($account->uuid) {
                $sentData['uuid'] = $account->uuid;
            }

            $response = ActionManager::send($sentData, $url, $method);
            if ($response && isset($response['uuid'])) {

                Yii::$app->db->createCommand()
                    ->update('tiktok_account', [
                        'uuid' => $response['uuid'],
                        'tiktok_status' => self::STATUS_AWAITING],
                        ['id' => $this->id])
                    ->execute();

            }
        }
    }


    public function afterDelete()
    {

        if ($this->uuid) {
            $sentData['uuid'] = $this->uuid;

            ActionManager::send($sentData, ActionManager::ACCOUNT_URL, 'DELETE');
        }

        parent::afterDelete();
    }


    public static function check($account)
    {
        if ($account->uuid) {
            $sentData = [
                "uuid" => $account->uuid,
            ];

            $responseData = ActionManager::send($sentData, ActionManager::ACCOUNT_CHECK_URL, 'GET');

            if ($responseData && isset($responseData['is_active'])) {

                ActionManager::changeUserStatus($account->id, $responseData['is_active'] );

                if ($responseData['is_active'] == self::STATUS_ACTIVE) {
                    ActionManager::changeLastAuth($account->id);
                }
            }


        }

    }


}


