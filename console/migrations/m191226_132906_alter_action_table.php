<?php

use yii\db\Migration;

/**
 * Class m191226_132906_alter_action_table
 */
class m191226_132906_alter_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%action}}', 'whom_following_user_id', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191226_132906_alter_action_table cannot be reverted.\n";

        return false;
    }
    */
}
