<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\TiktokAccount;

/* @var $this yii\web\View */
/* @var $model common\models\TiktokAccount */

$this->title = $model->nickname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiktok Accounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tiktok-account-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' =>  Yii::t('app','Вы действительно хотите удалить?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nickname',
            'email:email',
            'url:url',
            'uuid',
            'secure_id',
            'tiktok_id',
            'device_id',
            'fp',
            'iid',
            'openud_id',
            'max_repost',
            'max_like',
            'max_view',
            'max_follower',
            'max_comment',
            'from_completed',
            'to_completed',
            'last_login',
            [
                'attribute' => 'tiktok_status',
                'value' => function ($model) {
                    return $model->tiktok_status ? TiktokAccount::getStatus()[$model->tiktok_status] : '';
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y'],
            ],
        ],
    ]) ?>

</div>
