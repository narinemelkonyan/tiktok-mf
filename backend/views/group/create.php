<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $composite \backend\forms\group\Composite */

$this->title = Yii::t('app', 'Создание новой группы ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'composite' => $composite,
    ]) ?>

</div>
