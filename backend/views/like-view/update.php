<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\LikeForm */

$this->title = Yii::t('app', 'Редактирование {name}', [
    'name' => $model->video_url,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Like View Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="like-view-form-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
