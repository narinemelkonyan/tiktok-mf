<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\RePostForm */

$this->title = Yii::t('app', 'Создать действие  репост');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Re Post Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="re-post-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
