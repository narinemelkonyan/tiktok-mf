<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\forms\action\SubscribeForm;

/**
 * SubscribeSearch represents the model behind the search form of `backend\forms\action\SubscribeForm`.
 */
class SubscribeSearch extends SubscribeForm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tiktok_account_id', 'min_follower', 'max_follower', 'repeat_count', 'created_at', 'type_id'], 'integer'],
            [['whom_following_nickname', 'video_url', 'text'], 'safe'],
            [['is_subscribe_follower'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubscribeForm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tiktok_account_id' => $this->tiktok_account_id,
            'is_subscribe_follower' => $this->is_subscribe_follower,
            'min_follower' => $this->min_follower,
            'max_follower' => $this->max_follower,
            'repeat_count' => $this->repeat_count,
            'created_at' => $this->created_at,
            'type_id' => $this->type_id,
        ]);

        $query->andFilterWhere(['ilike', 'whom_following_nickname', $this->whom_following_nickname])
            ->andFilterWhere(['ilike', 'video_url', $this->video_url])
            ->andFilterWhere(['ilike', 'text', $this->text]);

        return $dataProvider;
    }
}
