<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use \yii\db\ActiveRecord;


/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 *
 * @property TiktokAccount[] $tiktokAccounts
 */
class Group extends ActiveRecord
{
    public $relationsWasChanged = false;
    public $accounts;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%group}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at'], 'required'],
            [['created_at'], 'default', 'value' => null],
            [['created_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'created_at' => Yii::t('app', 'Дата создания'),
            'accounts' => '',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiktokAccounts()
    {
        return $this->hasMany(TiktokAccount::class, ['group_id' => 'id']);
    }

    public static function getGroups()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
