<?php
use yii\helpers\Url;
use yii\widgets\Menu;
?>
<!-- BEGIN Left Aside -->

<aside class="page-sidebar">
    <div class="page-logo">
        <a href="<?= Url::to(['/tiktok-account/index']) ?>" class="page-logo-link press-scale-down d-flex align-items-center position-relative" data-toggle="modal" data-target="#modal-shortcut">
            <img src="/build/img/logo.png"  aria-roledescription="logo">
            <span class="page-logo-text mr-1"><?= Yii::t('app','TikTok MassFollowing')?></span>
            <span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
        </a>
    </div>
    <!-- BEGIN PRIMARY NAVIGATION -->
    <nav id="js-primary-nav" class="primary-nav" role="navigation">
        <div class="nav-filter">
            <div class="position-relative">
                <input type="text" id="nav_filter_input" placeholder="Filter menu" class="form-control" tabindex="0">
                <a href="#" onclick="return false;" class="btn-primary btn-search-close js-waves-off" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar">
                    <i class="fal fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <?php

        $actionDirectories = [
            [
                'label' => Yii::t('app','Подписки'),
                'url' => ['/subscribe'],
                'visible' => true,
                'active' => Yii::$app->controller->id == 'subscribe',
                'options' => ['class' => ''],
                'template' => '<a href="{url}" class="sidebar-link"><span>{label}</span></a>',
                'activeCssClass' => 'active',
            ],
            [
                'label' => Yii::t('app','Лайк / Просмотр'),
                'url' => ['/like-view'],
                'visible' => true,
                'active' => Yii::$app->controller->id == 'like-view',
                'options' => ['class' => ''],
                'template' => '<a href="{url}" class="sidebar-link"><span>{label}</span></a>',
                'activeCssClass' => 'active',
            ],
//            [
//                'label' => Yii::t('app','Комментарии'),
//                'url' => ['/comment'],
//                'visible' => true,
//                'active' => Yii::$app->controller->id == 'comment',
//                'options' => ['class' => ''],
//                'template' => '<a href="{url}" class="sidebar-link"><span>{label}</span></a>',
//                'activeCssClass' => 'active',
//            ],
//            [
//                'label' => Yii::t('app','Репосты'),
//                'url' => ['/re-post'],
//                'visible' => true,
//                'active' => Yii::$app->controller->id == 're-post',
//                'options' => ['class' => ''],
//                'template' => '<a href="{url}" class="sidebar-link"><span>{label}</span></a>',
//                'activeCssClass' => 'active',
//            ],
        ];
        echo Menu::widget([
            'encodeLabels' => false,
            'options' => ['class' => 'nav-menu', 'id' => 'js-nav-menu'],
            'items' => [
                [
                    'label' => Yii::t('app', 'Аккаунты'),
                    'url' => ['/tiktok-account'],
                    'template' => '<a href="{url}"  class="sidebar-link"> <i class="fal fa-user"></i><span >{label}</span></a>',
                    'visible' => true,
                    'options' => ['class' => ''],
                    'active' => Yii::$app->controller->id == 'tiktok-account',
                    'activeCssClass' => 'active',
                ],
                [
                    'label' => Yii::t('app', 'Группы'),
                    'url' => ['/group'],
                    'template' => '<a href="{url}"  class="sidebar-link"> <i class="fal fa-users"></i><span >{label}</span></a>',
                    'visible' => true,
                    'options' => ['class' => ''],
                    'active' => Yii::$app->controller->id == 'group',
                    'activeCssClass' => 'active',
                ],
                [
                    'label' => Yii::t('app', 'Действия'),
                    'url' => '#',
                    'template' => '<a href="{url}"  class="sidebar-link"> <i class="fal fa-cog"></i><span >{label}</span></a>',
                    'items' => $actionDirectories,
                    'options' => ['class' => 'open'],
                    'submenuTemplate' => "\n<ul id='dashboards' class='sidebar-dropdown list-unstyled collapse' role='menu'>\n{items}\n</ul>\n",
                    'activeCssClass' => 'active',
                ],
                [
                    'label' => Yii::t('app', 'Задания'),
                    'url' => ['/task'],
                    'template' => '<a href="{url}"  class="sidebar-link"> <i class="fal fa-tasks"></i><span >{label}</span></a>',
                    'visible' => true,
                    'options' => ['class' => ''],
                    'active' => Yii::$app->controller->id == 'task',
                    'activeCssClass' => 'active',
                ],
                [
                    'label' => Yii::t('app', 'Прокси'),
                    'url' => ['/proxy'],
                    'template' => '<a href="{url}"  class="sidebar-link"> <i class="fal fa-cloud"></i><span >{label}</span></a>',
                    'visible' => true,
                    'options' => ['class' => ''],
                    'active' => Yii::$app->controller->id == 'proxy',
                    'activeCssClass' => 'active',
                ],


            ],
            'activateParents' => true,
            'activeCssClass' => 'active',
        ]);

        ?>
    </nav>
    <!-- END PRIMARY NAVIGATION -->
    <!-- NAV FOOTER -->

</aside>
<!-- END Left Aside -->
