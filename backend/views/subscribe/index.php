<?php

use yii\helpers\Html;
use yii\grid\GridView;
use  \common\components\grid\ActionColumn;
use  \common\models\Setting;
use \backend\forms\action\SubscribeForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SubscribeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Подписки');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-form-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Задать подписку'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            ['attribute' => 'tiktokAccountNickname',
                'label' => Yii::t('app', 'Аккаунт исполнителя'),
                'value' => function ($model) {
                    return $model->tiktokAccount->nickname;
                }
            ],
            'whom_following_nickname',
            [
                'attribute' => 'type_id',
                'filter' => SubscribeForm::getType(),
                'value' => function ($model) {
                    return SubscribeForm::getType()[$model->type_id];
                }
            ],
            [
                'label' => Yii::t('app', 'Дата действия'),
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->startDate,
                    'attribute' => 'startDate',
                    'pluginOptions' => [
                        'orientation' => 'bottom'
                    ]
                ]),
                'value' => function ($model) {
                    return $model->tasks ? date("Y-m-d H:i", strtotime($model->tasks[0]->start_at)) : null;
                }
            ],
//            [
//                'format' => 'raw',
//                'label' =>'Отписаться ',
//                'value' => function ($model) {
//                    $action = '';
//                    if ($model->type_id == Setting::TYPE_SUBSCRIBE) {
////                        $action = '<a href="' . \yii\helpers\Url::to(['unsubscribe', 'id' => $model->id]) .'"><button class="btn btn-warning">Отписаться</button></a>';
//                        $action = '<a><button class="btn btn-warning">Отписаться</button></a>';
//                    }
//                    return $action;
//                },
//            ],
            [
                'class' => ActionColumn::class,
                'contentOptions' => ['class' => 'action-column'],
                'visibleButtons' => [
                    'update' => function($model){
                   return $model->tasks ? false : true;
                    },
                    'delete' => function($model){
                        return $model->tasks ? false : true;
                    },
                ],

            ],
        ],
    ]); ?>


</div>
