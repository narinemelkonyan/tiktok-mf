<?php

use yii\db\Migration;

/**
 * Class m191224_083251_alter_tiktok_account_table
 */
class m191224_083251_alter_tiktok_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tiktok_account}}', 'tiktok_id', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tiktok_account}}', 'tiktok_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191224_083251_alter_tiktok_account_table cannot be reverted.\n";

        return false;
    }
    */
}
