<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \backend\forms\action\RePostForm;
use kartik\select2\Select2;
use common\models\Group;
use common\models\TiktokAccount;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\RePostForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="re-post-form-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tiktok_account_id')->widget(Select2::class, [
        'data' => TiktokAccount::getAccounts(),
        'options' => ['prompt' => Yii::t('app', 'Выберите аккаунт'), 'multiple' => false],
        'pluginOptions' => [
            'tags' => false,
            'allowClear' => true,
            'tokenSeparators' => [',', ' '],
        ],
    ])->label();
    ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'groupId')->widget(Select2::class, [
            'data' => Group::getGroups(),
            'options' => ['prompt' => Yii::t('app', 'Выберите аккаунт'), 'multiple' => false],
            'pluginOptions' => [
                'tags' => false,
                'allowClear' => true,
                'tokenSeparators' => [',', ' '],
            ],
        ])->label(Yii::t('app', 'Или выберите группу '));
        ?>
    <?php endif;   ?>

    <?= $form->field($model, 'video_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 're_post_type_id')->radioList(RePostForm::getType(),
        [
            'itemOptions' => [
                'labelOptions' => [
                    'class' => 're-post-label',
                ],
            ]
        ]
        )->label(false) ?>

    <?= $form->field($model, 're_post_count')->textInput(['type'=>'number']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
