<?php

use yii\db\Migration;

/**
 * Class m191226_113605_alter_proxi_table
 */
class m191226_113605_alter_proxi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%proxi}}');
        $this->createTable('{{%proxi}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'IP' => $this->string()->notNull(),
            'port' => $this->string()->notNull(),
            'status' => $this->integer()->null(),
            'check_date' => $this->text()->null(),
            'error_message' => $this->text()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191226_113605_alter_proxi_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191226_113605_alter_proxi_table cannot be reverted.\n";

        return false;
    }
    */
}
