<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\TiktokAccount;

class TiktokAccountController extends Controller
{

    public function actionCheck()
    {
        $currentDate =  strtotime('-2 minutes');
        $accounts = TiktokAccount::find()
            ->where(['tiktok_status' => TiktokAccount::STATUS_AWAITING])
            ->andWhere(['NOT', ['uuid' => null]])
            ->andWhere(['<', 'created_at', $currentDate])
            ->all();

        if ($accounts) {
            foreach ($accounts as $account) {
                TiktokAccount::check($account);
            }
        }
    }

}
