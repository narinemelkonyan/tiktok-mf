<?php

namespace common\models;

use backend\services\ProxyCheckerService;
use common\components\parser\LoadException;
use Yii;
use yii\web\NotFoundHttpException;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "proxi".
 *
 * @property int $id
 * @property int $login
 * @property int $password
 * @property int $IP
 * @property int $port
 * @property string|null $check_date
 * @property int $status
 * @property int $last_used_date
 * @property string|null $error_message
 */
class Proxy extends ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%proxi}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password', 'IP', 'port'], 'required'],
            [['IP'], 'unique'],
            ['IP', 'ip'],
            [['login', 'password', 'IP', 'port', 'status', 'last_used_date'], 'default', 'value' => null],
            [['check_date', 'error_message', 'login', 'password', 'IP', 'port'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('app', 'Логин'),
            'password' => Yii::t('app', 'Пароль'),
            'IP' => Yii::t('app', 'IP'),
            'port' => Yii::t('app', 'Порт'),
            'check_date' => Yii::t('app', 'Последняя проверка'),
            'status' => Yii::t('app', 'Статус   '),
            'error_message' => Yii::t('app', 'Сообщение'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->status = self::STATUS_INACTIVE;
        }

        return parent::beforeSave($insert);
    }

    public static function getStatus()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Активный'),
            self::STATUS_INACTIVE => Yii::t('app', 'Неактивный'),
        ];
    }

    public function getActive()
    {
        $proxy = Proxy::find()
            ->where(['status' => Proxy::STATUS_ACTIVE])
            ->orderBy(['last_used_date' => SORT_ASC])
            ->one();
        if ($proxy) {
            $proxyChecker = new ProxyCheckerService();
            $address = $proxy->IP . ':' . $proxy->port;

            $content = $proxyChecker->getProxyContent($address, $proxy->login, $proxy->password);

            if ($content) {
                $proxy->last_used_date = strtotime(date("Y-m-d H:i:s"));
                $proxy->save();
                return $proxy;
            } else {
                $proxy->status = self::STATUS_INACTIVE;
                $proxy->check_date = date("Y-m-d H:i:s");
                $proxy->last_used_date = strtotime(date("Y-m-d H:i:s"));
                $proxy->save();
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Нет активных прокси'));
            throw new NotFoundHttpException(Yii::t('app', 'Нет  активных прокси'));
        }

        return $this->getActive();

    }

}
