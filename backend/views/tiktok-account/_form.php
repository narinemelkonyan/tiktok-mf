<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use  \common\models\Setting;

/* @var $this yii\web\View */
/* @var $model common\models\TiktokAccount */
/* @var $form yii\widgets\ActiveForm */

?>
<style>
    #custom_graph {
        display: none;
    }
</style>
<div class="tiktok-account-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if (!$model->isNewRecord): ?>
        <?= $form->field($model, 'nickname')->textInput(['maxlength' => true, 'disabled' => true]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'disabled' => !$model->isNewRecord,'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'device_id')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'fp')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'iid')->textInput(['maxlength' => true,'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'openud_id')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <p class="label_bold"><?= Yii::t('app', 'Лимиты на действия аккаунта в сутки') ?></p>

    <?= $form->field($model, 'max_repost')->textInput(['placeholder' => Yii::t('app', 'Введите максимальное число')]) ?>

    <?= $form->field($model, 'max_like')->textInput(['placeholder' => Yii::t('app', 'Введите максимальное число')]) ?>

    <?= $form->field($model, 'max_view')->textInput(['placeholder' => Yii::t('app', 'Введите максимальное число')]) ?>

    <?= $form->field($model, 'max_follower')->textInput(['placeholder' => Yii::t('app', 'Введите максимальное число')]) ?>

    <div id="schedule_type">
        <p class="label_bold"><?= Yii::t('app', 'График выполнения действии') ?></p>
        <div class="bottom-10"><input type="radio" name="schedule_type" value="0">
            <span class="schedule_label">
            <?= Yii::t('app', 'По стандарту: (') . Setting::FROM_COMPLETED . ' - ' . Setting::TO_COMPLETED . ')' ?>
        </span>
        </div>
        <div class="bottom-10">
            <input type="radio" name="schedule_type" value="1">
            <span class="schedule_label"><?= Yii::t('app', 'Задать своё ') ?></span>
        </div>
    </div>
    <div id="custom_graph" <?= $model->to_completed != '00:00' && $model->from_completed != '00:00' ? 'style="display:block"' : '' ?>>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'from_completed')->widget(TimePicker::class,
                    [
                        'pluginOptions' => [
                            'showMeridian' => false,
                            'allowClear' => true,
                            'defaultTime' => '',
                        ],
                    ]); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'to_completed')->widget(TimePicker::class,
                    [
                        'pluginOptions' => [
                            'showMeridian' => false,
                            'allowClear' => true,
                            'defaultTime' => '',
                        ]
                    ]); ?>
            </div>
        </div>
        <p style="display: none" id="shc_error" class="help-block"><?= Yii::t('app', 'Выберите график ') ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success create-accounts']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$script = <<< JS

$('form').submit(function () {

    var schedule_type = $("#schedule_type input[type=radio]:checked").val();
    if (schedule_type == 0) {
        $('#shc_error').hide();
        $('#tiktokaccount-from_completed').val('00:00');
        $('#tiktokaccount-to_completed').val('00:00');
        return true;
    } else if (schedule_type == 1) {
       var from =   $('#tiktokaccount-from_completed').val();
      var to =  $('#tiktokaccount-to_completed').val();
     
      if(from == '00:00' || to == '00:00'){
          $('#shc_error').show();
          return false;
      }else{
            $('#shc_error').hide();
             return true;
      }
        
    }

});

JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>

