<?php


namespace backend\forms\import;

use common\models\File;
use common\models\Proxy;
use common\models\TiktokAccount;
use common\modules\storage\models\UploadForm;

use ruskid\csvimporter\ARImportStrategy;
use yii\base\Model;
use yii\helpers\FileHelper;
use ruskid\csvimporter\CSVImporter;
use ruskid\csvimporter\CSVReader;


class CsvImportForm extends Model
{

    const DELIMITER = ';';
    public $file;



    public function rules()
    {
        return [
            ['file', 'file', 'skipOnEmpty' => false, 'extensions' => 'csv']
        ];
    }


    public function upload($dir)
    {
        if (FileHelper::createDirectory($dir)) {
            $newName = \Yii::$app->security->generateRandomString(10) . '.csv';

            if ($this->file->saveAs($dir . $newName)) {
                return $newName;
            }

        }
    }


    public function importProxyData($fileName){
        $importer = new CSVImporter();

        $importer->setData(new CSVReader([
            'filename' => \Yii::getAlias('@csv-proxy') . $fileName,
            'fgetcsvOptions' => [
                'delimiter' => self::DELIMITER
            ]
        ]));

        $numberRowsAffected = $importer->import(new ARImportStrategy([
            'className' => Proxy::class,
            'configs' => [

                [
                    'attribute' => 'login',
                    'value' => function ($line) {
                        return isset($line[0]) ?  utf8_encode($line[0]) : null;
                    }
                ],
                [
                    'attribute' => 'password',
                    'value' => function ($line) {
                        return isset($line[1]) ?  utf8_encode($line[1]) : null;
                    }
                ],
                [
                    'attribute' => 'IP',
                    'value' => function($line) {
                        return isset($line[2]) ?  utf8_encode($line[2]) : null;
                    },
                    'unique' => true,
                ],
                [
                    'attribute' => 'port',
                    'value' => function ($line) {
                        return isset($line[3]) ?  utf8_encode($line[3]) : null;
                    }
                ],

            ],

        ]));

        return $numberRowsAffected;
    }

    public function importAccountData($fileName){

        $importer = new CSVImporter();

         $importer->setData(new CSVReader([
            'filename' => \Yii::getAlias('@csv-accounts') . $fileName,
            'fgetcsvOptions' => [
                'delimiter' => self::DELIMITER
            ]
        ]));

        $numberRowsAffected = $importer->import(new ARImportStrategy([
            'className' => TiktokAccount::class,
            'configs' => [
                [
                    'attribute' => 'url',
                    'value' => function ($line) {
                        return isset($line[0]) ? utf8_encode($line[0]) : null;
                    }
                ],
                [
                    'attribute' => 'email',
                    'value' => function ($line) {
                        return isset($line[1]) ? utf8_encode($line[1]) : null;
                    }
                ],
                [
                    'attribute' => 'password',
                    'value' => function($line) {
                        return isset($line[2]) ? utf8_encode($line[2]) : null;
                    }
                ],
                [
                    'attribute' => 'device_id',
                    'value' => function ($line) {
                        return isset($line[3]) ? utf8_encode($line[3]) : null;
                    }
                ],
                [
                    'attribute' => 'fp',
                    'value' => function ($line) {
                        return isset($line[4]) ? utf8_encode($line[4]) : null;
                    }
                ],
                [
                    'attribute' => 'iid',
                    'value' => function ($line) {
                        return isset($line[5]) ? utf8_encode($line[5]) : null;
                    }
                ],
                [
                    'attribute' => 'openud_id',
                    'value' => function ($line) {
                        return isset($line[6]) ? utf8_encode($line[6]) : null;
                    }
                ],
                [
                    'attribute' => 'max_repost',
                    'value' => function ($line) {
                        return isset($line[7]) ? utf8_encode($line[7]) : null;
                    }
                ],
                [
                    'attribute' => 'max_like',
                    'value' => function ($line) {
                        return isset($line[8]) ? utf8_encode($line[8]) : null;
                    }
                ],
                [
                    'attribute' => 'max_follower',
                    'value' => function ($line) {
                        return isset($line[9]) ? utf8_encode($line[9]) : null;
                    }
                ],
                [
                    'attribute' => 'schedule_type',
                    'value' => function ($line) {
                        return isset($line[10]) ? utf8_encode($line[10]) : null;
                    }
                ],
                [
                    'attribute' => 'from_completed',
                    'value' => function ($line) {
                        return isset($line[11]) ? utf8_encode($line[11]) : null;
                    }
                ],
                [
                    'attribute' => 'to_completed',
                    'value' => function ($line) {
                        return isset($line[12]) ? utf8_encode($line[12]) : null;
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function () {
                        return time();
                    }
                ]
            ],

        ]));

        return $numberRowsAffected;
    }
}