<?php

namespace common\components\parser;

use common\models\Proxy;
use Yii;
use const http\Client\Curl\Versions\CURL;

abstract class Loader
{
    protected $url = 'https://www.tiktok.com/';

    public function get_string_between($string, $start, $end)
    {
        $string = " " . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public static function getProtocols()
    {
        return [
            CURLPROXY_HTTP => 'HTTP',
            CURLPROXY_SOCKS5 => 'SOCKS5',
        ];
    }

    /**
     * @param $url
     * @return bool|string
     * @throws LoadException
     */
    public function getContent($url)
    {
        $model = new Proxy();
        $address = $model->getActive();

        if ($address) {

            $userAgent = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31';
            $request_headers = [
                'User-Agent: ' . $userAgent,
            ];

            $proxy = $address->IP . ':' . $address->port;
            $login = $address->login;
            $password = $address->password;
            $type = 0;

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            if ($proxy) {
                curl_setopt($ch, CURLOPT_PROXY, $proxy);
            }
            if ($proxy && $login && $password) {
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $login . ':' . $password);
            }
            if ($proxy && $login && $password && $type) {
                curl_setopt($ch, CURLOPT_PROXYTYPE, $type);
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            $output = curl_exec($ch);

            curl_close($ch);
            if (!$output) {
                throw new LoadException(Yii::t('app', 'Ошибка при загрузке страницы ') . $url);
            }
            return $output;
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Нет  активных прокси'));
            throw new NotFoundHttpException(Yii::t('app', 'Нет  активных прокси'));
        }


    }

    /**
     * @param string $url
     * @return TtData
     * @throws ParserException
     */
    abstract public function parse(string $url): TtData;
}
