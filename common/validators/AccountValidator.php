<?php
namespace common\validators;

use common\components\parser\LoadException;
use common\components\parser\ParserException;
use common\components\parser\ProfileParser;

use common\models\TiktokAccount;

use Yii;
use yii\validators\ValidationAsset;
use yii\validators\Validator;

class AccountValidator extends Validator
{
    public $message;

    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = \Yii::t('app', 'Введенная Вами ссылка на аккаунт в TikTok неверна');
        }
    }

    /**
     * @param TiktokAccount $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;

        $account = null;
        try {
            $loader = new ProfileParser();
            $uname = $loader->getProfilePageLink($value);
            $account = $loader->parse($uname);
        }catch (\Exception $e) {
            $account = null;
        }
        if (!$account) {
            $model->addError($attribute, $this->message);
        } else {
            $model->nickname = $account->uniqueId;
            $model->follower = $account->follower;
            $model->secure_id = $account->secUid;
            $model->tiktok_id = $account->user_id;

            return $model;
        }
    }
}
