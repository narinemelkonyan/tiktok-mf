`docker-compose up -d`

`docker-compose exec php-fpm php init`

`docker-compose exec php-fpm composer install`


Настройки соединения с БД при работе в файле:
`common\config\main-local.php`
```
'db' => [
 'class' => 'yii\db\Connection',
 'dsn' => 'pgsql:host=db;port=5432;dbname=tiktok_mf_db',
 'username' => 'tiktok_mf_user',
 'password' => 'y46ryr7Y5',
 'charset' => 'utf8',
]
```

`docker-compose exec php-fpm php yii migrate`
