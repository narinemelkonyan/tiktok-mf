<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $composite \backend\forms\group\Composite */

$this->title = Yii::t('app', 'Редактирование группы');

?>
<div class="group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'composite' => $composite,
    ]) ?>

</div>
