<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \backend\forms\action\RePostForm;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\RePostForm */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Re Post Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="re-post-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute' => 'tiktok_account_id',
                'value' => function ($model) {
                    return $model->tiktokAccount->nickname;
                }
            ],
            'video_owner_nickname',
            'video_url:url',
            'video_id',
            [
                'attribute' => 're_post_type_id',

                'value' => function ($model) {
                    return RePostForm::getType()[$model->re_post_type_id];
                }
            ],
            're_post_count',
            [
                'label' => Yii::t('app', 'Дата подписки'),
                'value' => function () {
                    $data = '';
                    return $data;
                }
            ],
        ],
    ]) ?>

</div>
