<?php

namespace backend\services;

use common\models\Action;
use common\models\Setting;
use common\models\Task;
use common\models\TiktokAccount;
use yii\db\Query;

class QueueManager
{
    public static function getActions()
    {
        $task = Task::find()->select(['action_id'])->asArray()->column();
        $db = new Query();
        $db->from(Action::tableName());
        $db->select(['action.id', 'action.tiktok_account_id', 'action.type_id', 'action.repeat_count', 'tiktok_account.from_completed', 'tiktok_account.to_completed', 'tiktok_account.max_like', 'tiktok_account.max_view','tiktok_account.max_follower']);
        $db->innerJoin(TiktokAccount::tableName(), "action.tiktok_account_id = tiktok_account.id");
        $db->andFilterWhere(['not in', Action::tableName() . '.id', $task]);
        $db->orderBy(['from_completed' => SORT_ASC]);

        return $db->all();
    }

    public static function getStartDate()
    {
        $task = Task::find()->orderBy(['start_at' => SORT_DESC])->one();

        $startDate = date('Y-m-d H:i');

        if ($task && self::isCurrentTime($task->start_at)) {

            $startDate = $task->start_at;
        }

        return $startDate;
    }


    public static function getStartAt($startDate)
    {
        return date("Y-m-d H:i", strtotime($startDate) + (Setting::TIMEOUT * 60));
    }

    public static function isInSchedule($fromComplete, $toComplete, $date)
    {
        $fromComplete = date('H:i', strtotime($fromComplete));
        $toComplete = date('H:i', strtotime($toComplete));
        $date = date('H:i', strtotime($date));

        return ($date >= $fromComplete) && ($date <= $toComplete);
    }

    public static function isInLimit($actionType, $accountId, $maxLike, $maxView, $maxFollower)
    {
        $db = new Query();
        $db->from(Task::tableName());
        $db->select(['task.id']);
        $db->innerJoin(Action::tableName(), "task.action_id = action.id");
        $db->andFilterWhere(['action.tiktok_account_id' => $accountId]);
        $db->andFilterWhere(['action.type_id' => $actionType]);
        $db->andFilterWhere(['ilike', 'task.start_at', date("Y-m-d")]);
        $count = $db->count();

        if($actionType == Setting::TYPE_LIKE ){
            return $count < $maxLike;
        }
        if($actionType == Setting::TYPE_VIEW){
            return $count < $maxView;
        }
        if($actionType == Setting::TYPE_SUBSCRIBE){
            return $count < $maxFollower;
        }

    }

    public static function isCurrentTime($start)
    {
        $start = date('Y-m-d H:i', strtotime($start));
        $current = date('Y-m-d H:i', strtotime(date('Y-m-d H:i')));

        return $start > $current;
    }

    public function isExistTime($time)
    {

        return Task::find()->where(['start_at' => $time])->one();

    }


}