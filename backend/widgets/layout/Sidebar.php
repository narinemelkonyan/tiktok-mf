<?php

namespace  backend\widgets\layout;

use yii\base\Widget;
use yii\helpers\Url;

/**
 * Class Sidebar
 * @package app\components\layout
 */
class Sidebar extends Widget
{
    /**
     * @var string
     */
    public $layout;

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();
    }

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        if ($this->layout !== null) {
            return $this->render($this->layout);
        }

        return $this->render('sidebar');
    }


}
