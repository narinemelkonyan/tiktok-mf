<?php

use yii\db\Migration;

/**
 * Class m191225_095248_alter_tiktok_task_table
 */
class m191225_095248_alter_tiktok_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%task}}', 'start_at', $this->string()->notNull());
        $this->alterColumn('{{%task}}', 'execute_at', $this->string()->null());
        $this->alterColumn('{{%task}}', 'completed_at', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191225_095248_alter_tiktok_task_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191225_095248_alter_tiktok_task_table cannot be reverted.\n";

        return false;
    }
    */
}
