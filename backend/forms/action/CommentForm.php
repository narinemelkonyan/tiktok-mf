<?php


namespace backend\forms\action;


use common\models\Action;
use common\models\Setting;
use common\models\TiktokAccount;
use Yii;
use yii\helpers\ArrayHelper;

class CommentForm extends Action
{

    public $groupId;

    public function rules()
    {
        return [
            [['tiktok_account_id', 'groupId', 'video_url'], 'isAccountCheck'],
            [['type_id', 'created_at', 'video_url', 'text'], 'required'],
            [['tiktok_account_id', 'repeat_count', 'created_at'], 'default', 'value' => null],
            [['tiktok_account_id', 'repeat_count', 'created_at'], 'integer'],
            [['video_url', 'text'], 'string', 'max' => 255],
            [['video_url'], 'url'],
            ['video_url', 'common\validators\VideoValidator'],
            [['repeat_count'], 'number', 'min' => 0],
            [['tiktok_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => TiktokAccount::class, 'targetAttribute' => ['tiktok_account_id' => 'id']],
        ];
    }


    public function isAccountCheck()
    {
        if ($this->tiktok_account_id == '' && $this->groupId == '') {
            $this->addError('tiktok_account_id', $this->getAttributeLabel(''));
            $this->addError('groupId', $this->getAttributeLabel('Необходимо выбрать аккаунт или группу'));
        }
    }

    public function beforeValidate()
    {
        $this->created_at = time();
        $this->type_id = Setting::TYPE_COMMENT;

        return parent::beforeValidate();
    }

    private function getCommentText($comment)
    {
        if ($comment) {
            $comments = preg_split('/\r\n|[\r\n]/', $comment);
            $comments = array_filter($comments);
            $comments = array_unique($comments);
            return $comments;
        }

    }


    public function saveActions()
    {
        $comments = $this->getCommentText($this->text);

        if ($comments) {

            foreach ($comments as $comment) {

                if ($this->groupId) {

                    $accounts = TiktokAccount::find()->where(['group_id' => $this->groupId])->all();

                    if ($accounts) {

                        foreach ($accounts as $account) {

                            $this->add($account->id, $this->video_url, $comment, $this->repeat_count);
                        }
                    }

                } elseif ($this->tiktok_account_id) {

                    $this->add($this->tiktok_account_id, $this->video_url, $comment, $this->repeat_count);

                }
            }
        }
    }

    protected function add($account_id, $videoUrl, $text, $repeatCount)
    {
        $action = new Action();
        $action->video_url = $videoUrl;
        $action->tiktok_account_id = $account_id;
        $action->text = $text;
        $action->repeat_count = $repeatCount;
        $action->type_id = Setting::TYPE_COMMENT;
        $action->created_at = time();
        $action->save();
    }
}