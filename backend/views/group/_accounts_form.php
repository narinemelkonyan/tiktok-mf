<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use  \common\models\TiktokAccount;
/* @var $this yii\web\View */
/* @var $items array */
/* @var $form yii\widgets\ActiveForm */
/* @var int $parent_id */

$items = $model->items;
$all_items = $model->all_items;
$error = $model->getErrors('item');
$error = implode(', ', $error);
?>
<h4 class="label_bold color-success-500"><?= Yii::t('app', 'Аккаунты') ?></h4>
<div class="multiform-wrapper">
    <div class="multiform-items">


        <?php if ($all_items): ?>
            <div>
                <input type="checkbox" id="check_all" value="1">
                <span class="accounts_label_all"><?= Yii::t('app', 'Выбрать все ') ?></span>
            </div>

            <?php foreach ($all_items as $key => $item):
                if($item->tiktok_status == TiktokAccount::STATUS_ACTIVE):
                ?>
                <div class="row multiform-item">
                    <div class="col-xs-10 all-accounts">

                        <?= $form->field(new \common\models\Group(), "accounts[$key]", [
                            'template' => '{input}{label}',
                            'options' => ['class' => 'form-group form-inline'],
                        ])->checkbox([
                            'uncheck' => false,
                            'value' => $item->id,
                            'checked' => $item->group_id && $item->group_id == $parent_id
                        ])->label('<span class="accounts_label">' . $item->nickname . '</span>') ?>

                    </div>
                </div>
            <?php endif; endforeach; else: ?>

            <p class="color-danger-800"><?= Yii::t('app', 'Нет свободных аккаунтов ') ?></p>
        <?php endif; ?>
    </div>
    <?php if ($error): ?>
        <p class="help-block"><?= $error ?></p>
    <?php endif; ?>

</div>

