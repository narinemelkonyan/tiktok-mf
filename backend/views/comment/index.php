<?php

use yii\helpers\Html;
use yii\grid\GridView;
use  \common\components\grid\ActionColumn;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Комментарии');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-form-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать действия', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'id' => 'comment-table',
            'class' => 'table table-striped table-bordered'
        ],
        'columns' => [
            'id',
            ['attribute' => 'tiktokAccountNickname',
                'label' => Yii::t('app', 'Аккаунт исполнителя'),
                'value' => function ($model) {
                    return $model->tiktokAccount->nickname;
                }
            ],

            [
                'label' => Yii::t('app', 'Дата действия'),
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->startDate,
                    'attribute' => 'startDate',
                    'pluginOptions' => [
                        'orientation' => 'bottom'
                    ]
                ]),
                'value' => function ($model) {
                    return $model->tasks ? date("Y-m-d H:i", strtotime($model->tasks[0]->start_at)) : '';
                }
            ],
            'video_url:url',
            'text',
            [
                'class' => ActionColumn::class,
                'contentOptions' => ['class' => 'action-column'],
            ],
        ],
    ]); ?>


</div>
