<?php

use yii\helpers\Html;
use yii\grid\GridView;
use  \common\components\grid\ActionColumn;
use \common\models\Task;
use kartik\date\DatePicker;
use \common\models\Setting;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Задания');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'nickname',
                'label' => Yii::t('app', 'TikTok аккаунт'),
                'value' => function ($model) {
                    return $model->action->tiktokAccount->nickname;
                }
            ],
            [
                'attribute' => 'actionType',
                'label' => Yii::t('app','Тип'),
                'filter' => Setting::getActionType(),
                'value' => function ($model) {
                    return Setting::getActionType()[$model->action->type_id];
                }
            ],
            [
                'label' => Yii::t('app', 'Дата создания действия'),
                'format' => ['date', 'dd.MM.Y'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->createdDate,
                    'attribute' => 'createdDate',
                    'pluginOptions' => [
                        'orientation' => 'bottom'
                    ]
                ]),
                'value' => function ($model) {
                    return $model->action->created_at;
                }
            ],
            [
                'attribute' => 'start_at',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->start_at,
                    'attribute' => 'start_at',
                    'pluginOptions' => [
                        'orientation' => 'bottom'
                    ]
                ]),
            ],
            [
                'attribute' => 'status',
                'filter' => Task::getStatus(),
                'value' => function ($model) {
                    return Task::getStatus()[$model->status];
                }
            ],
            [
                'class' => ActionColumn::class,
                'contentOptions' => ['class' => 'action-column'],
                'template' => '{view}{delete}',
                'visibleButtons' => [
                    'delete' => function($model){
                        return $model->uuid ? false : true;
                    },
                ],
            ],
        ],
    ]); ?>


</div>
