<?php

use yii\db\Migration;

/**
 * Class m191227_114139_alter_action_table
 */
class m191227_114139_alter_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->addColumn('{{%action}}', 'whom_following_user_id', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191227_114139_alter_action_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191227_114139_alter_action_table cannot be reverted.\n";

        return false;
    }
    */
}
