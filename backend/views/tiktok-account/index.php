<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use \common\models\TiktokAccount;
use  \common\components\grid\ActionColumn;
use \backend\forms\import\CsvImportForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TiktokAccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Аккаунты');
$this->params['breadcrumbs'][] = $this->title;
$import = new CsvImportForm();
?>
<div class="tiktok-account-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-2">
            <p>
                <?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success btn-proxi']) ?>
            </p>
        </div>
        <div class="col-md-6">
            <?= $this->render('_import', ['model' => $import]); ?>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'nickname',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'dd.MM.Y'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->created_at,
                    'attribute' => 'created_at',
                    'pluginOptions' => [
                        'orientation' => 'bottom'
                    ]
                ]),
            ],
            [
                'attribute' => 'last_login',
                'format' => ['date', 'dd.MM.Y'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->last_login,
                    'attribute' => 'last_login',
                    'pluginOptions' => [
                        'orientation' => 'bottom'
                    ]
                ]),
            ],
            [
                'attribute' => 'tiktok_status',
                'filter' => TiktokAccount::getStatus(),
                'value' => function ($model) {
                    return $model->tiktok_status ? TiktokAccount::getStatus()[$model->tiktok_status] : '';
                }
            ],
            [
                'class' => ActionColumn::class,
                'contentOptions' => ['class' => 'action-column'],
            ],

        ],
    ]); ?>

</div>
