<?php

namespace common\models\search;

use common\models\Action;
use common\models\TiktokAccount;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Task;

/**
 * TaskSearch represents the model behind the search form of `common\models\Task`.
 */
class TaskSearch extends Task
{
    public $nickname;
    public $actionType;
    public $createdDate;

    public $start;
    public $end;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'action_id', 'status', 'execute_at', 'completed_at'], 'integer'],
            [['createdDate', 'actionType', 'nickname', 'start', 'end', 'start_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->createdDate) {
            $date = new \DateTimeImmutable($this->createdDate);
            $this->start = $date->format('U');
            $this->end = $date->modify('+24 hours')->format('U');
        }


        $query->innerJoin(Action::tableName(), Task::tableName() . '.action_id =' . Action::tableName() . '.id');
        $query->innerJoin(TiktokAccount::tableName(), Action::tableName() . '.tiktok_account_id =' . TiktokAccount::tableName() . '.id');


        // grid filtering conditions
        $query->andFilterWhere([
            Task::tableName() . '.id' => $this->id,
            'action_id' => $this->action_id,
            'status' => $this->status,
            'execute_at' => $this->execute_at,
            'completed_at' => $this->completed_at,
        ]);
        if ($this->createdDate) {
            $query->andWhere(['BETWEEN', Action::tableName() . '.created_at', $this->start, $this->end]);
        }

        if ($this->start_at) {
            $query->andFilterWhere(['ilike', 'start_at', date("Y-m-d", strtotime($this->start_at))]);
        }

        $query->andFilterWhere(['ilike', TiktokAccount::tableName() . '.nickname', $this->nickname]);
        $query->andFilterWhere(['=', Action::tableName() . '.type_id', $this->actionType]);


        return $dataProvider;
    }
}
