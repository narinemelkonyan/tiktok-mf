<?php

use \yii\helpers\Html;

?>
<header class="page-header" role="banner">
    <div class="hidden-lg-up">
        <a href="#" class="header-btn btn press-scale-down waves-effect waves-themed" data-action="toggle" data-class="mobile-nav-on">
            <i class="ni ni-menu"></i>
        </a>
    </div>
    <div class="ml-auto d-flex">
        <div>
            <?= Html::a('Выход', ['/site/logout'], ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']); ?>
        </div>
    </div>
</header>
