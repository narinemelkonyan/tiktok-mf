<?php

use yii\db\Migration;

/**
 * Class m200120_140613_alter_tiktok_account_table
 */
class m200120_140613_alter_tiktok_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tiktok_account}}', 'max_view', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tiktok_account}}', 'max_view');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200120_140613_alter_tiktok_account_table cannot be reverted.\n";

        return false;
    }
    */
}
