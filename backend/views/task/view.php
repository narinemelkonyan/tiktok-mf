<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Setting;
use common\models\Task;
/* @var $this yii\web\View */
/* @var $model common\models\Task */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'uuid',
            [
                'attribute' => 'nickname',
                'label' => Yii::t('app', 'TikTok аккаунт'),
                'value' => function ($model) {
                    return $model->action->tiktokAccount->nickname;
                }
            ],
            [
                'attribute' => 'actionType',
                'label' => Yii::t('app','Тип'),
                'filter' => Setting::getActionType(),
                'value' => function ($model) {
                    return Setting::getActionType()[$model->action->type_id];
                }
            ],
           'start_at',
            [
                'label' => Yii::t('app', 'Дата создания действия'),
                'format' => ['date', 'php:d.m.Y'],
                'value' => function ($model) {
                    return $model->action->created_at;
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Task::getStatus(),
                'value' => function ($model) {
                    return Task::getStatus()[$model->status];
                }
            ],
        ],
    ]) ?>

</div>
