<?php

namespace backend\forms\action;

use common\models\Action;
use common\models\Setting;
use common\models\TiktokAccount;
use common\validators\AccountValidator;
use Yii;

class RePostForm extends Action
{
    const TYPE_ALL = 1;
    const TYPE_RANDOM = 2;
    public $groupId;


    public function rules()
    {
        return [
            [['tiktok_account_id', 'groupId', 'video_url'], 'isAccountCheck'],
            [['type_id', 'created_at', 'video_url', 're_post_type_id'], 'required'],
            [['re_post_type_id', 're_post_count'], 'isRePostCountCheck'],
            [['tiktok_account_id', 'created_at'], 'default', 'value' => null],
            [['tiktok_account_id', 'created_at', 're_post_type_id', 're_post_count'], 'integer'],
            [['video_url', 'text'], 'string', 'max' => 255],
            [['video_url'], 'url'],
            ['video_url', 'common\validators\VideoValidator'],
            [['re_post_count'], 'number', 'min' => 1],
            [
                ['tiktok_account_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => TiktokAccount::class,
                'targetAttribute' => ['tiktok_account_id' => 'id']
            ],

        ];

    }


    public function isAccountCheck()
    {
        if ($this->tiktok_account_id == '' && $this->groupId == '') {
            $this->addError('tiktok_account_id', $this->getAttributeLabel(''));
            $this->addError('groupId', $this->getAttributeLabel('Необходимо выбрать аккаунт или группу'));
        }
    }

    public function isRePostCountCheck()
    {

        $isOk = false;
        if ($this->groupId) {
            $accounts = TiktokAccount::find()->where(['group_id' => $this->groupId])->all();
            $isOk = true;
            if ($accounts) {
                foreach ($accounts as $account) {
                    $this->getMinMaxFollowerCount($account);
                }
            }

        } elseif ($this->tiktok_account_id) {
            $isOk = true;
            $account = TiktokAccount::findOne($this->tiktok_account_id);
            $this->getMinMaxFollowerCount($account);
        }


        $min = 0;
        if (Yii::$app->session->has('follower')) {
            $count = Yii::$app->session->get('follower');
            $min = min($count);
        }

        if ($isOk && $this->re_post_type_id == self::TYPE_RANDOM && $this->re_post_count > $min) {
            $this->addError('re_post_count',
                $this->getAttributeLabel('Значение «Количество(чел.)» должно быть не больше  ' . $min));
        } elseif ($this->re_post_type_id == self::TYPE_RANDOM && !$this->re_post_count) {

            $this->addError('re_post_count', $this->getAttributeLabel('Необходимо заполнить «Количество(чел.)»'));

        }
        Yii::$app->session->set('follower', null);

    }

    public function beforeValidate()
    {
        $this->created_at = time();
        $this->type_id = Setting::TYPE_REPOST;

        return parent::beforeValidate();
    }


    public function saveActions()
    {
        if ($this->groupId) {

            $accounts = TiktokAccount::find()->where(['group_id' => $this->groupId])->all();

            if ($accounts) {
                foreach ($accounts as $account) {
                    $this->add($account->id, $this->video_url, $this->video_id, $this->video_owner_nickname,
                        $this->re_post_type_id, $this->re_post_count);
                }
            }

        } elseif ($this->tiktok_account_id) {

            $this->save();
        }

    }

    protected function add($account_id, $videoUrl, $video_id, $video_owner_nickname, $rePostType, $rePostCount)
    {
        $action = new Action();
        $action->video_url = $videoUrl;
        $action->tiktok_account_id = $account_id;
        $action->video_owner_nickname = $video_owner_nickname;
        $action->video_id = $video_id;
        $action->re_post_type_id = $rePostType;
        $action->re_post_count = $rePostCount;
        $action->created_at = time();
        $action->type_id = Setting::TYPE_REPOST;
        $action->save();
    }

    public static function getType()
    {
        return [
            self::TYPE_ALL => Yii::t('app', 'Всем'),
            self::TYPE_RANDOM => Yii::t('app', 'Рандомно'),
        ];
    }

    private function getMinMaxFollowerCount($account)
    {
        if ($account) {
            $validator = new AccountValidator();
            $account = $validator->validateAttribute($account, 'url');
            $this->saveFollowerCount($account->follower);
        }

    }

    private function saveFollowerCount($follower)
    {
        if ($follower) {
            $count = [];
            if (Yii::$app->session->has('follower')) {
                $count = Yii::$app->session->get('follower');
            }
            array_push($count, $follower);
            Yii::$app->session->set('follower', $count);
        }
    }


}