<?php

use yii\db\Migration;

/**
 * Class m191219_102512_alter_action_table
 */
class m191219_102512_alter_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%action}}', 'type_id');
        $this->addColumn('{{%action}}', 'type_id', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191219_102512_alter_action_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191219_102512_alter_action_table cannot be reverted.\n";

        return false;
    }
    */
}
