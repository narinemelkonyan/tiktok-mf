<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use \backend\widgets\layout\Sidebar;
use \backend\widgets\layout\TopBar;
use lo\modules\noty\Wrapper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="mod-bg-1">
<?php $this->beginBody() ?>

<div class="page-wrapper">
    <div class="page-inner">
        <?= Sidebar::widget(); ?>
        <div class="page-content-wrapper">

            <?= \backend\widgets\layout\TopBar::widget(); ?>
            <div id="noty-layer"></div>
            <?= Wrapper::widget([
                'layerClass' => 'lo\modules\noty\layers\Noty',
                'layerOptions' => [
                    'layerId' => 'noty-layer',
                    'customTitleDelimiter' => '|',
                    'overrideSystemConfirm' => false,
                    'showTitle' => true,
                ],
                'options' => [
                    'dismissQueue' => true,
                    'layout' => 'topRight',
                    'timeout' => 3000,
                    //'theme' => 'bootstrapTheme',
                    'animation' => [
                        'open' => 'animated bounceInRight',
                        'close' => 'animated bounceOutRight'
                    ]
                ],
            ]);
            ?>
            <main id="js-page-content" role="main" class="page-content">
                <?= $content ?>
            </main>
            <div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on"></div>

            <footer class="page-footer" role="contentinfo">
                <div class="d-flex align-items-center flex-1 text-muted">
                    <span class="hidden-md-down fw-700">2019 © TikTok MassFollowing&nbsp;</span>
                </div>
            </footer>
        </div>
    </div>
</div>

<footer class="footer">
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
