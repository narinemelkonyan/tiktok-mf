<?php

use yii\db\Migration;

/**
 * Class m191226_132847_alter_tiktok_account_table
 */
class m191226_132847_alter_tiktok_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

         $this->addColumn('{{%tiktok_account}}', 'uuid', $this->string()->null());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tiktok_account}}', 'uuid');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191226_132847_alter_tiktok_account_table cannot be reverted.\n";

        return false;
    }
    */
}
