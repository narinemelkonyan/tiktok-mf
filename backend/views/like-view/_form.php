<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\TiktokAccount;
use common\models\Group;
use kartik\select2\Select2;
use \backend\forms\action\LikeForm;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\LikeForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="like-view-form-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'video_url')->textInput(['maxlength' => true, 'autocomplete' => 'off']) ?>

    <?= $form->field($model, 'tiktok_account_id')->widget(Select2::class, [
        'data' => TiktokAccount::getAccounts(),
        'options' => ['prompt' => Yii::t('app', 'Выберите аккаунт'), 'multiple' => false],
        'pluginOptions' => [
            'tags' => false,
            'allowClear' => true,
            'tokenSeparators' => [',', ' '],
        ],
    ])->label();
    ?>


    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'groupId')->widget(Select2::class, [
            'data' => Group::getGroups(),
            'options' => ['prompt' => Yii::t('app', 'Выберите аккаунт'), 'multiple' => false],
            'pluginOptions' => [
                'tags' => false,
                'allowClear' => true,
                'tokenSeparators' => [',', ' '],
            ],
        ])->label(Yii::t('app', 'Или выберите группу '));
        ?>


        <?= $form->field($model, 'type_id')->checkboxList(LikeForm::getType(),
            [
                'itemOptions' => [
                    'labelOptions' => [
                        'class' => 'like-view-label',
                    ],
                ]
            ]) ?>

    <?php else: ?>

        <?= $form->field($model, "type_id", [
            'template' => '{input}{label}',
            'options' => ['class' => 'form-group form-inline'],
        ])->checkbox([
            'uncheck' => false,
            'value' => $model->type_id,
            'checked' => true,
            'disabled' => true,
            'label' => '',
        ])->label('<span class="like_label">' . LikeForm::getType()[$model->type_id] . '</span>') ?>


    <?php endif; ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
