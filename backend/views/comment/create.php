<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\CommentForm */

$this->title = Yii::t('app','Создать действие комментарий');
$this->params['breadcrumbs'][] = ['label' => 'Comment Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
