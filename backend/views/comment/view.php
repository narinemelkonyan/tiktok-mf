<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\CommentForm */

$this->title = $model->video_url;
$this->params['breadcrumbs'][] = ['label' => 'Comment Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="comment-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Вы действительно хотите удалить?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute' => 'tiktok_account_id',
                'value' => function ($model) {
                    return $model->tiktokAccount->nickname;
                }
            ],
            'video_url:url',
            'text',
            'repeat_count',

            [
                'label' => Yii::t('app', 'Дата подписки'),
                'value' => function () {
                    $data = '';
                    return $data;
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y'],
            ],
        ],
    ]) ?>

</div>
