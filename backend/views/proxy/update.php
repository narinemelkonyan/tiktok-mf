<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Proxy */

$this->title = Yii::t('app', 'Редактирование: {name}', [
    'name' => $this->title = $model->IP . ':' . $model->port,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proxys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="Proxy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
