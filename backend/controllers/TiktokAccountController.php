<?php

namespace backend\controllers;

use backend\forms\import\CsvImportForm;
use backend\services\ActionManager;
use Yii;
use common\models\TiktokAccount;
use common\models\search\TiktokAccountSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use \yii\web\ServerErrorHttpException;

/**
 * TiktokAccountController implements the CRUD actions for TiktokAccount model.
 */
class TiktokAccountController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view','import'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TiktokAccount models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TiktokAccountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TiktokAccount model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TiktokAccount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TiktokAccount();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TiktokAccount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldAttributes = $model->oldAttributes;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $isChange = $model->attributes == $oldAttributes;
            if (!$isChange) {
                $model->addAccountOnTikTOk($model, ActionManager::ACCOUNT_URL, 'PATCH');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TiktokAccount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch (\Exception $e) {
            throw new ServerErrorHttpException(Yii::t('app',
                'Невозможно удалить аккаунт, так как к ней привязаны действия.'));
        }

        return $this->redirect(['index']);
    }

    public function actionImport()
    {
        $model = new CsvImportForm();
        $model->file = UploadedFile::getInstance($model, 'file');

        if ($model->file) {
            $csvFile = $model->upload( \Yii::getAlias('@csv-accounts'));

            if ($csvFile) {
                $accounts = $model->importAccountData($csvFile);
                Yii::$app->session->setFlash('info', \Yii::t(
                    'app',
                    '{n, plural, =0{Добавлено # аккаунтов} one{Добавлен # аккаунт} few{Добавлено # аккаунта} other{Добавлено # аккаунтов}}',
                    ['n' =>  count($accounts)]
                ));
            }

        }
        $this->redirect('index');
    }


    /**
     * Finds the TiktokAccount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TiktokAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TiktokAccount::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


}
