<?php


namespace common\components\parser;


use common\models\Action;


use yii\helpers\Json;

class VideoParser extends Loader
{

    public function parse(string $url): TtData
    {
        $content = $this->getContent($url);
        $data = trim($this->get_string_between($content, '"videoData":', ',"shareUser"'));
        $res = Json::decode(substr($data, 0));
        if (!$res){
            throw new ParserException(\Yii::t('app', 'Ошибка при парсинге страницы видео. '. $url));
        }

        $data = new Video();
        $data->videoDescription = $res['itemInfos']['text'];
        $data->videoId = $res['itemInfos']['id'];
        $data->tikTokName = '@' . $res['authorInfos']['uniqueId'];

        return $data;
    }

    /**
     * @param $url
     * @param null $tikTokName
     * @return string
     * @throws LoadException
     * @throws ParserException
     */
    public function getVideoPageLink($url, $tikTokName = null)
    {
        $pattern = '~/(@[\w,\.\-]+)/(?:video)/(\d+)(?:\.|\?|$)~';
        preg_match($pattern, $url, $matches);
        if ($matches){
            return $this->url . $matches[1] . '/video/' . $matches[2];
        }

//        else {
//            return $this->url . $tikTokName . '/video/' . $url;
//        }
        $content = $this->getContent($url);
        $content = trim($content);
        $pattern = '/<a\s+href\s*=\s*"(.+?)"\s*>.*?<\/a>.$/';
        $res = preg_match($pattern, $content, $matches);
        if (!$res){
            throw new ParserException(\Yii::t('app', 'Неверная ссылка на страницу с видео.' . $url));
        }
        return $this->getVideoPageLink($matches[1]);
    }

    /**
     * @param string $url

     *
     * @return TtData
     * @throws LoadException
     * @throws ParserException
     * @throws VideoDescriptionException
     * @throws VideoTikTokNameException
     */
    public function validateVideo(string $url)
    {
        $loader = new self();
        $url = $loader->getVideoPageLink($url);
        $res = $loader->parse($url);


        return $res;
    }


    /**
     * @param string $string
     * @return string|string[]|null
     */
    protected function trimString(string $string)
    {
        return preg_replace('/\s+/', '', $string);
    }
}
