<?php

namespace backend\services;


class ProxyCheckerService
{
    private $proxyCheckUrl = 'https://www.tiktok.com/';

    private $request_headers = [
        'User-Agent: ' => 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31',
    ];


    public  function getProxyContent($proxy, $login, $password, $type = 0)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->proxyCheckUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        if ($proxy) {
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
        }
        if ($proxy && $login && $password) {

            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $login . ':' . $password);
        }
        if ($proxy && $login && $password && $type) {

            curl_setopt($ch, CURLOPT_PROXYTYPE, $type);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->request_headers);
        $output = curl_exec($ch);

        curl_close($ch);

        return $output;

    }


}
