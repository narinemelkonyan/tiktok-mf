<?php

namespace common\models\search;

use common\models\Task;
use common\models\TiktokAccount;
use PHPUnit\Util\Type;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Action;

/**
 * ActionSearch represents the model behind the search form of `common\models\Action`.
 */
class ActionSearch extends Action
{
    public $tiktokAccountNickname;
    public $startDate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'tiktok_account_id', 'follower_count', 'repeat_count', 'created_at', 'type_id'], 'integer'],
            [['whom_following_nickname', 'video_url', 'text', 'tiktokAccountNickname', 'video_owner_nickname','startDate'], 'safe'],
            [['is_subscribe_follower'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param array $type
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type)
    {
        $query = Action::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->startDate) {
            $query->leftJoin(Task::tableName(), Action::tableName() . '.id = ' . Task::tableName() . '.action_id');
        }

        if ($this->tiktokAccountNickname) {
            $query->innerJoin(TiktokAccount::tableName(), Action::tableName() . '.tiktok_account_id = ' . TiktokAccount::tableName() . '.id');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_subscribe_follower' => $this->is_subscribe_follower,
            'follower_count' => $this->follower_count,
            'repeat_count' => $this->repeat_count,
            'created_at' => $this->created_at,
            'type_id' => $this->type_id,
        ]);

        $query->andFilterWhere(['in', 'type_id', $type])
            ->andFilterWhere(['ilike', 'whom_following_nickname', $this->whom_following_nickname])
            ->andFilterWhere(['ilike', 'video_url', $this->video_url])
            ->andFilterWhere(['ilike', 'text', $this->text])
            ->andFilterWhere(['ilike', 'video_owner_nickname', $this->video_owner_nickname])
            ->andFilterWhere(['like', TiktokAccount::tableName() . '.nickname', $this->tiktokAccountNickname]);

        if ($this->startDate) {
            $query->andFilterWhere(['ilike', Task::tableName() . '.start_at', date("Y-m-d", strtotime($this->startDate))]);
        }



        return $dataProvider;
    }
}
