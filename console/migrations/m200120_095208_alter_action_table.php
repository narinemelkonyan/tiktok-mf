<?php

use yii\db\Migration;

/**
 * Class m200120_095208_alter_action_table
 */
class m200120_095208_alter_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%action}}', 'min_follower');
        $this->dropColumn('{{%action}}', 'max_follower');

        $this->addColumn('{{%action}}', 'follower_count', $this->integer()->null());


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%action}}', 'min_follower', $this->integer()->null());
        $this->addColumn('{{%action}}', 'max_follower', $this->integer()->null());

        $this->dropColumn('{{%action}}', 'follower_count');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200120_095208_alter_action_table cannot be reverted.\n";

        return false;
    }
    */
}
