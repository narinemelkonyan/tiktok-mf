<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Proxy;
use backend\services\ProxyCheckerService;

class ProxyController extends Controller
{

    public function actionChecker()
    {
        $proxy = Proxy::find()->all();
        if ($proxy) {
            $proxyChecker = new ProxyCheckerService();
            foreach ($proxy as $item) {
                $proxy = $item->IP . ':' . $item->port;
                $results = $proxyChecker->getProxyContent($proxy, $item->login, $item->password);
                if ($results) {
                    $status = Proxy::STATUS_ACTIVE;
                } else {
                    $status = Proxy::STATUS_INACTIVE;
                }
                $item->status = $status;
                $item->last_used_date = strtotime(date("Y-m-d H:i:s"));
                $item->check_date = date("Y-m-d H:i:s");
                $item->save();
            }
        }

    }


}
