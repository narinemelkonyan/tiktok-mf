<?php

use yii\db\Migration;

/**
 * Class m191220_133800_alter_action_table
 */
class m191220_133800_alter_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%action}}', 'repeat_count', $this->integer()->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191220_133800_alter_action_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191220_133800_alter_action_table cannot be reverted.\n";

        return false;
    }
    */
}
