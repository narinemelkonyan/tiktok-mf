<?php

namespace backend\forms\group;

use common\models\Group;
use common\models\TiktokAccount;
use Yii;
use yii\base\Model;


class AccountsForm extends Model
{
    public $updateExist = false;
    public $parentModel;

    public $isChanged = false;
    public $items = [];
    public $all_items = [];
    protected $forDelete = [];


    public function __construct(Group $model, $config = [])
    {
        $this->parentModel = $model;

        if($model->isNewRecord){
            $this->all_items = TiktokAccount::find()
                ->where(['group_id' => null])
                ->andWhere(['tiktok_status'=>TiktokAccount::STATUS_ACTIVE])->all();
        }else{
            $this->all_items = TiktokAccount::find()
                ->where(['group_id' => $model->id])
                ->orWhere(['group_id' => null])
                ->all();
        }
        $this->items = $model->getTiktokAccounts()->all();

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            ['items', 'validateData'],
        ];
    }

    public function validateData($attribute)
    {
        if ($this->items === null) {
            $this->addError('item', \Yii::t('app', 'Необходимо выбрать аккаунты '));
            return false;
        }
        if ($this->items && count($this->items) < 2) {

            $this->addError('item', \Yii::t('app', 'Необходимо выбрать минимум 2 аккаунта'));
            return false;
        }
        return true;
    }

    protected function getExist($incData)
    {
//        foreach ($this->items as $item) {
//            if ($item->id == $incData['id']) {
//                return $item;
//            }
//        }

        return new TiktokAccount();
    }

    public function save()
    {


        $res = false;
        if ($this->validateData('items')) {

            $res = true;

            self::unlink($this->parentModel->id);

            foreach ($this->items as $item) {
                   $this->link($item, $this->parentModel->id);
            }

        }
        return $res;
    }

    public static function delete($groupId){
        self::unlink($groupId);
    }

    public static  function link($accountId, $groupId = null)
    {
        $res = false;
        if (($model = TiktokAccount::findOne($accountId)) !== null) {

            $model->group_id = $groupId;
            if ($model->save(false)) {
                $res = true;
            }
        }
        return $res;
    }

    public static function unlink($groupId)
    {
      $accounts = TiktokAccount::find()->where(['group_id' =>$groupId])->all();

      if($accounts){
          foreach ($accounts as $account){
              self::link($account->id);
          }
      }

    }


}