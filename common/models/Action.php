<?php

namespace common\models;

use Yii;
use yii\httpclient\Client;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "action".
 *
 * @property int $id
 * @property int $type_id
 * @property int $tiktok_account_id
 * @property string|null $whom_following_nickname
 * @property string|null $whom_following_user_id
 * @property string|null $whom_following_url
 * @property bool|null $is_subscribe_follower
 * @property int|null $follower_count
 * @property string|null $video_url
 * @property string|null $video_id
 * @property string|null $video_owner_nickname
 * @property string|null $text
 * @property int|null $repeat_count
 * @property int $created_at
 * @property int $re_post_type_id
 * @property int $re_post_count
 *
 * @property TiktokAccount $tiktokAccount
 * @property Task[] $tasks
 */
class Action extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%action}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'tiktok_account_id', 'created_at'], 'required'],
            [['tiktok_account_id', 'follower_count', 'repeat_count', 'created_at'], 'default', 'value' => null],
            [['tiktok_account_id', 'follower_count',  'repeat_count', 'created_at', 're_post_type_id', 're_post_count'], 'integer'],
            [['is_subscribe_follower'], 'boolean'],
            [['whom_following_nickname', 'video_url', 'text', 'video_owner_nickname', 'video_id'], 'string', 'max' => 255],
            [['tiktok_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => TiktokAccount::class, 'targetAttribute' => ['tiktok_account_id' => 'id']],


            [[ 'whom_following_url'],
                'unique', 'targetAttribute' => ['tiktok_account_id', 'whom_following_url'],
                'message' => Yii::t('app', 'Комбинация - параметров Аккаунт исполнителя и Подписаться на (Ссылка) уже существует.')
            ],
            [[ 'video_url'],
                'unique', 'targetAttribute' => ['tiktok_account_id', 'video_url', 'type_id'],
                'message' => Yii::t('app', 'Комбинация - параметров Аккаунт исполнителя и ссылка на видео уже существует')
            ]





        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Тип'),
            'tiktok_account_id' => Yii::t('app', 'Аккаунт исполнителя'),
            'whom_following_nickname' => Yii::t('app', 'Подписаться на'),
            'whom_following_url' => Yii::t('app', 'Подписаться на (Ссылка)'),
            'is_subscribe_follower' => Yii::t('app', 'Подписаться на подписчиков'),
            'follower_count' => Yii::t('app', 'Количество подписчиков, на которых надо  подписаться'),
            'video_url' => Yii::t('app', 'Ссылка на видео'),
            'text' => Yii::t('app', 'Текст комментариев'),
            'repeat_count' => Yii::t('app', 'Количество повторов'),
            'created_at' => Yii::t('app', 'Дата создания'),
            're_post_count' => Yii::t('app', 'Количество (чел.)'),
            're_post_type_id' => Yii::t('app', 'Репостнуть кому'),
            'video_owner_nickname' => Yii::t('app', 'Владелец ролика'),
            'video_id' => Yii::t('app', 'Видео ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiktokAccount()
    {
        return $this->hasOne(TiktokAccount::class, ['id' => 'tiktok_account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::class, ['action_id' => 'id']);
    }


}
