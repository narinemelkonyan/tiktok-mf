<?php

use yii\db\Migration;
use common\models\User;
/**
 * Class m200207_083100_update_admin_password
 */
class m200207_083100_update_admin_password extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $password = Yii::$app->security->generatePasswordHash('afh&DT457MN@');

        Yii::$app->db
            ->createCommand("UPDATE " . User::tableName() ." SET password_hash = '" . $password ."' WHERE username = 'admin'")
            ->execute();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200207_083100_update_admin_password cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200207_083100_update_admin_password cannot be reverted.\n";

        return false;
    }
    */
}
