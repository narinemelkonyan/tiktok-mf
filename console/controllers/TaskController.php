<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Task;
use common\models\Setting;
use backend\forms\action\SubscribeForm;
use backend\forms\action\LikeForm;

class TaskController extends Controller
{

    public function actionCheck()
    {
        $tasks = Task::find()
            ->where(['status' => Task::STATUS_WAITING])
            ->andWhere(['NOT', ['uuid' => null]])
            ->all();

        if ($tasks) {

            foreach ($tasks as $task) {
                Task::check($task);
            }
        }

    }


    public function actionAddInBot()
    {
        $currentDate = date('Y-m-d H:i');

        $tasks = Task::find()
            ->where(['status' => Task::STATUS_WAITING])
            ->andWhere(['uuid' => null])
            ->andWhere(['<', 'start_at', $currentDate])
            ->all();
        if ($tasks) {
            foreach ($tasks as $task) {

                if ($task->action->type_id == Setting::TYPE_SUBSCRIBE) {

                    SubscribeForm::addSubscribeInBot($task);
                }

                if ($task->action->type_id == Setting::TYPE_VIEW || $task->action->type_id == Setting::TYPE_LIKE) {

                    LikeForm::addSubscribeInBot($task);
                }

            }
        }

    }

    public function actionCreateQueue()
    {
        $task = new Task();
        $task->composite();
    }

}
