<?php

namespace  backend\widgets\layout;

use backend\components\Panel;
use common\models\User;
use Yii;
use yii\base\Widget;

/**
 * Class TopBar
 * @package app\components\layout
 */
class TopBar extends Widget
{
    public $message;

    /**
     * @var string
     */
    public $layout;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if ($this->layout !== null) {
            return $this->render($this->layout);
        }
        return $this->render('top-bar',[
            'user' => User::findOne(['id' => Yii::$app->user->id])
        ]);
    }
}
