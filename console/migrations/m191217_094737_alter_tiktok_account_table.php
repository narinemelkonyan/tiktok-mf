<?php

use yii\db\Migration;

/**
 * Class m191217_094737_alter_tiktok_account_table
 */
class m191217_094737_alter_tiktok_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tiktok_account}}', 'max_comment', $this->integer()->null());
        $this->alterColumn('{{%tiktok_account}}', 'to_completed', $this->string(5)->null());
        $this->alterColumn('{{%tiktok_account}}', 'from_completed', $this->string(5)->null());
        $this->alterColumn('{{%tiktok_account}}', 'secure_id', $this->string()->null());
        $this->dropColumn('{{%tiktok_account}}', 'password');
        $this->addColumn('{{%tiktok_account}}', 'password', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tiktok_account}}', 'max_comment');
        $this->alterColumn('{{%tiktok_account}}', 'to_completed', $this->integer()->null());
        $this->alterColumn('{{%tiktok_account}}', 'from_completed', $this->integer()->null());
        $this->alterColumn('{{%tiktok_account}}', 'secure_id', $this->string()->notNull());
        $this->alterColumn('{{%tiktok_account}}', 'password', $this->string()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191217_094737_alter_tiktok_account_table cannot be reverted.\n";

        return false;
    }
    */
}
