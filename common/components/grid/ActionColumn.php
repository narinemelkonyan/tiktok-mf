<?php


namespace common\components\grid;

use yii\grid\ActionColumn as DefActionColumn;
use yii\helpers\Html;
use Yii;

class ActionColumn extends DefActionColumn
{
    public $template = '{view}{update}{delete}';

    protected function initDefaultButtons()
    {
        $this->initDefaultButton('view', 'eye');
        $this->initDefaultButton('update', 'pencil');
        $this->initDefaultButton('delete', 'times-circle', [
            'data-confirm' => Yii::t('app', 'Вы действительно хотите удалить?'),
            'data-method' => 'post',
            'class' => 'label-danger'
        ]);
    }

    /**
     * Initializes the default button rendering callback for single button.
     * @param string $name
     * @param string $iconName
     * @param array $additionalOptions
     */
    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Update');
                        break;
                    case 'delete':
                        $title = Yii::t('app', 'Удалить');
                        break;
                    default:
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'title' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0',
                ], $additionalOptions, $this->buttonOptions);

                $icon = Html::tag('i', '', ['class' => 'fa fa-'.$iconName,'data-feather' => $iconName]);
                return Html::a($icon, $url, $options);
            };
        }
    }

}
