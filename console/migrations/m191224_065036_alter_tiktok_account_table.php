<?php

use yii\db\Migration;

/**
 * Class m191224_065036_alter_tiktok_account_table
 */
class m191224_065036_alter_tiktok_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%tiktok_account}}', 'url', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191224_065036_alter_tiktok_account_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191224_065036_alter_tiktok_account_table cannot be reverted.\n";

        return false;
    }
    */
}
