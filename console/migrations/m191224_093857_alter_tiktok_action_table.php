<?php

use yii\db\Migration;

/**
 * Class m191224_093857_alter_tiktok_action_table
 */
class m191224_093857_alter_tiktok_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%action}}', 'video_id', $this->string()->null());
        $this->addColumn('{{%action}}', 'video_owner_nickname', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%action}}', 'video_id');
        $this->dropColumn('{{%action}}', 'video_owner_nickname');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191224_093857_alter_tiktok_action_table cannot be reverted.\n";

        return false;
    }
    */
}
