<?php

namespace common\validators;

use common\components\parser\ParserException;
use common\components\parser\Video;
use common\components\parser\VideoDescriptionException;
use common\components\parser\VideoParser;
use common\components\parser\VideoTikTokNameException;
use yii\base\Model;
use yii\validators\Validator;

class VideoValidator extends Validator
{
    public $message;

    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = \Yii::t('app', 'Введенная Вами ссылка на аккаунт в TikTok неверна');
        }
    }

    /**
     * @param Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $url = $model->$attribute;
        $res = null;
        $loader = new VideoParser();
        try {
            /** @var Video $res */
            $res = $loader->validateVideo($url);
        } catch (ParserException $e) {
            $model->addError($attribute, $e->getMessage());
        } catch (VideoDescriptionException $e) {
            $model->addError($attribute, $e->getMessage());
        } catch (VideoTikTokNameException $e) {
            $model->addError($attribute, $e->getMessage());
        }
        if ($res) {
            $model->video_url = 'https://www.tiktok.com/' . $res->tikTokName . '/video/' . $res->videoId;
            $model->video_id =  $res->videoId;
            $model->video_owner_nickname = $res->tikTokName;

        } else {
            $model->video_url = null;
        }


    }
}
