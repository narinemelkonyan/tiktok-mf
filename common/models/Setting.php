<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for settings.
 */
class Setting extends ActiveRecord
{

    const  FROM_COMPLETED = '08:00';
    const  TO_COMPLETED = '18:00';
    const  TIMEOUT = 1;

    const TYPE_SUBSCRIBE = 1;
    const TYPE_UNSUBSCRIBE = 2;
    const TYPE_LIKE = 3;
    const TYPE_VIEW = 4;
    const TYPE_COMMENT = 5;
    const TYPE_REPOST = 6;

    //Лимиты на действия аккаунта в сутки
    const MAX_LIKE = 2;
    const MAX_VIEW = 3;
    const MAX_SUBSCRIBE = 3;


    /**
     * @return array
     */
    public static function getActionType()
    {
        return [
            self::TYPE_SUBSCRIBE => Yii::t('app', 'Подписаться'),
            self::TYPE_UNSUBSCRIBE => Yii::t('app', 'Отписаться'),
            self::TYPE_LIKE => Yii::t('app', 'Лайк'),
            self::TYPE_VIEW => Yii::t('app', 'Просмотр'),
            self::TYPE_COMMENT => Yii::t('app', 'Комментария'),
            self::TYPE_REPOST => Yii::t('app', 'Репост'),
        ];
    }


}
