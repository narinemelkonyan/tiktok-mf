<?php


namespace backend\forms\action;

use backend\services\ActionManager;
use Yii;
use common\models\Action;
use common\models\Setting;
use common\models\TiktokAccount;


class SubscribeForm extends Action
{
    public $groupId;
//    public $whom_following_url;

    public $nickname;
    public $tiktok_id;
    public $follower;
    public $secure_id;


    public function rules()
    {
        return [
            [['tiktok_account_id', 'groupId', 'whom_following_url'], 'isAccountCheck'],
            [['tiktok_account_id', 'groupId'], 'safe'],
            [['type_id', 'created_at', 'whom_following_url'], 'required'],
            [['tiktok_account_id', 'follower_count', 'repeat_count', 'created_at'], 'default', 'value' => null],
            [['tiktok_account_id', 'follower_count', 'repeat_count', 'created_at', 'type_id', 'groupId'], 'integer'],
            [['is_subscribe_follower'], 'boolean'],
            [['whom_following_nickname', 'whom_following_user_id', 'video_url', 'text'], 'string', 'max' => 255],
            [['tiktok_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => TiktokAccount::class, 'targetAttribute' => ['tiktok_account_id' => 'id']],
            [['follower_count'], 'number', 'min' => 1, 'max' => 2000],
            ['whom_following_url', 'common\validators\AccountValidator'],

            [['tiktok_account_id'],
                'unique', 'targetAttribute' => ['tiktok_account_id', 'whom_following_url'],
                'message' => Yii::t('app', 'Комбинация параметров Аккаунт исполнителя и Подписаться на (Ссылка) уже существует.')
            ]
        ];
    }


    public function isAccountCheck($attribute)
    {
        if ($this->tiktok_account_id == '' && $this->groupId == '') {
            $this->addError('tiktok_account_id', $this->getAttributeLabel(''));
            $this->addError('groupId', $this->getAttributeLabel('Необходимо выбрать аккаунт или группу'));

        }
    }

    public function beforeValidate()
    {
        $this->type_id = Setting::TYPE_SUBSCRIBE;
        $this->created_at = time();

        return parent::beforeValidate();
    }

    public static function addSubscribeInBot($task)
    {
        if ($task) {

            $sentData = [
                'from' => $task->action->tiktokAccount->uuid,
                'point' => $task->action->whom_following_user_id
            ];
            if ($task->action->follower_count) {
                $sentData['subs_count'] = (int)$task->action->follower_count;
            }

            $responseData = ActionManager::send($sentData, ActionManager::SUBSCRIBE_URL);
            if ($responseData && isset($responseData['uuid'])) {
                ActionManager::addUuid($task, $responseData['uuid']);
            }
        }
    }


    public function saveSubscribe()
    {

        if ($this->groupId) {
            $accounts = TiktokAccount::find()->where(['group_id' => $this->groupId])->all();

            if ($accounts) {
                foreach ($accounts as $account) {
                    $this->add($account->id, $this->tiktok_id, $this->nickname, $this->whom_following_url, $this->follower_count);
                }
            }
        } elseif ($this->tiktok_account_id) {
            $this->add($this->tiktok_account_id, $this->tiktok_id, $this->nickname, $this->whom_following_url, $this->follower_count);
        }

    }


    protected function add($account_id, $whom_following_user_id, $whom_following_nickname, $whom_following_url, $follower_count)
    {
        $action = new Action();
        $action->created_at = time();
        $action->type_id = Setting::TYPE_SUBSCRIBE;
        $action->follower_count = $follower_count;
        $action->whom_following_nickname = $whom_following_nickname;
        $action->whom_following_url = $whom_following_url;
        $action->whom_following_user_id = $whom_following_user_id;
        $action->tiktok_account_id = $account_id;
        if ($action->validate()) {
            $action->save();
        }

    }


    public static function getType()
    {
        return [
            Setting::TYPE_SUBSCRIBE => Yii::t('app', 'Подписаться'),
            Setting::TYPE_UNSUBSCRIBE => Yii::t('app', 'Отписаться'),
        ];
    }
}