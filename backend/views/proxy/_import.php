<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Proxy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="import-form">

    <?php $form = ActiveForm::begin(['id' => 'import-csv', 'action' => Url::to('/proxy/import')]); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'file')->widget(FileInput::class, [
                'options' => ['multiple' => false],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showCancel' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'placeholder' => false,
                ]
            ])->label(false);
            ?>
            <p class="i-message"><?= Yii::t('app','Разрешена загрузка файла только с расширением CSV (разделитель - запятая). Первая строка обязательно должна содержать заголовки: логин, пароль, IP, хост') ?></p>
        </div>
        <div class="col-md-4  form-group">
            <?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'import-btn btn btn-dark']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>




