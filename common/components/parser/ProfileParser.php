<?php


namespace common\components\parser;


use yii\helpers\Json;

class ProfileParser extends Loader
{

    public function parse(string $url): TtData
    {
        $url = $this->url . $url;
        $content = $this->getContent($url);
        $data = trim($this->get_string_between($content, '"userData":', '"shareUser"'));
        $res = Json::decode(substr($data,0,-1));

        if (!$res){
            throw new ParserException(\Yii::t('app', 'Ошибка при парсинге страницы профиля. ' . $url));
        }
        $data = new Profile();
        $data->uniqueId = '@'.$res['uniqueId'];
        $data->secUid = $res['secUid'];
        $data->video = $res['video'];
        $data->follower = $res['fans'];
        $data->like = $res['heart'];
        $data->avatar = $res['coversMedium'][0];
        $data->user_id = $res['userId'];

        return $data;
    }

    public function getProfilePageLink($str)
    {
        $pattern = '~(?:tiktok\.com\/|^)(@[\w,\.\-]+)~';
        preg_match($pattern, $str, $matches);
        if ($matches){
            return $matches[1];
        }
        $content = $this->getContent($str);
        $content = trim($content);
        $pattern = '/<a\s+href\s*=\s*"(.+?)"\s*>.*?<\/a>.$/';
        $res = preg_match($pattern, $content, $matches);
        if (!$res){
            throw new ParserException(\Yii::t('app', 'Неверная ссылка на страницу профиля.' . $str));
        }
        return $this->getProfilePageLink($matches[1]);
    }
}
