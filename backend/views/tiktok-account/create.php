<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TiktokAccount */

$this->title = Yii::t('app', 'Добавить Аккаунт');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiktok Accounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiktok-account-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
