<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\Action1Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tiktok_account_id') ?>

    <?= $form->field($model, 'whom_following_nickname') ?>

    <?= $form->field($model, 'is_subscribe_follower')->checkbox() ?>

    <?= $form->field($model, 'min_follower') ?>

    <?php // echo $form->field($model, 'max_follower') ?>

    <?php // echo $form->field($model, 'video_url') ?>

    <?php // echo $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'repeat_count') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'type_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
