<?php

use yii\db\Migration;

/**
 * Class m191229_085149_add_uuid_to_task_table
 */
class m191229_085149_add_uuid_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%task}}', 'uuid',  $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%task}}', 'uuid');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191229_085149_add_uuid_to_task_table cannot be reverted.\n";

        return false;
    }
    */
}
