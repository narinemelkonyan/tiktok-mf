<?php

use yii\db\Migration;

/**
 * Class m200115_092811_alter_action_table
 */
class m200115_092811_alter_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%action}}', 'whom_following_url', $this->string()->null());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%action}}', 'whom_following_url');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200115_092811_alter_action_table cannot be reverted.\n";

        return false;
    }
    */
}
