<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\forms\action\SubscribeForm;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\SubscribeForm */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subscribe Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="subscribe-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= !$model->tasks ? Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= !$model->tasks ? Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить?'),
                'method' => 'post',
            ],
        ]) : ''?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute' => 'tiktok_account_id',
                'value' => function ($model) {
                    return $model->tiktokAccount->nickname;
                }
            ],
            'whom_following_nickname',
            'follower_count',
            [
                'label' => Yii::t('app', 'Дата подписки '),
                'value' => function ($model) {
                    return $model->tasks ? date("Y-m-d H:i", strtotime($model->tasks[0]->start_at)) : null;
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y'],
            ],

        ],
    ]) ?>

</div>
