<?php

namespace backend\forms\group;

use common\models\Group;
use yii\base\Model;
use Yii;

class Composite extends Model
{
    public $parentModel;
    public $accountsForm;

    public function __construct(Group $model, $config = [])
    {
        $this->parentModel = $model;
        $this->accountsForm = new AccountsForm($this->parentModel);

        parent::__construct($config);
    }

    public function load($data, $formName = null)
    {

        if (isset($data['Group'])) {
            $loadRes = true;
            $tmp = isset($data['Group']) ? $data['Group'] : [];

            $loadRes = $loadRes && $this->parentModel->load($tmp, '');

            return $loadRes;
        }
        return false;
    }

    public function save()
    {
        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $data = Yii::$app->request->post();

            if ($this->parentModel->isNewRecord) {
                $this->parentModel->created_at = time();
            }

            $parentRes = $this->parentModel->save();

            $this->accountsForm->items = isset($data['Group']['accounts']) ? $data['Group']['accounts'] : null ;
            $this->accountsForm->parentModel->id = $this->parentModel->id;
            $accountsRes = $this->accountsForm->save();

        } catch (\Throwable $e) {
            $transaction->rollBack();
            return false;
        }

        if ($parentRes && $accountsRes) {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollBack();
            return false;
        }

    }


}