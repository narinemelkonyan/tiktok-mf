<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TiktokAccount;
use DateTimeImmutable;
/**
 * TiktokAccountSearch represents the model behind the search form of `common\models\TiktokAccount`.
 */
class TiktokAccountSearch extends TiktokAccount
{
    public $start;
    public $end;

    public $startLogin;
    public $endLogin;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'max_repost', 'max_like', 'max_follower', 'registered_data',  'tiktok_status', 'group_id'], 'integer'],
            [['nickname', 'url', 'secure_id', 'email', 'password', 'device_id', 'fp', 'iid', 'openud_id','created_at', 'last_login', 'to_completed', 'from_completed',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TiktokAccount::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->created_at) {
            $date = new DateTimeImmutable($this->created_at);
            $this->start = $date->format('U');
            $this->end =  $date->modify('+24 hours')->format('U');
        }

        if ($this->last_login) {
            $date = new DateTimeImmutable($this->last_login);
            $this->startLogin = $date->format('U');
            $this->endLogin =  $date->modify('+24 hours')->format('U');
        }


//        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'registered_data' => $this->registered_data,
            'tiktok_status' => $this->tiktok_status,
        ]);
        if($this->created_at){
            $query->andWhere(['BETWEEN', 'created_at', $this->start, $this->end]);
        }
        if($this->last_login){
            $query->andWhere(['BETWEEN', 'last_login', $this->startLogin, $this->endLogin]);
        }
        $query->andFilterWhere(['ilike', 'nickname', $this->nickname])
            ->andFilterWhere(['between', TiktokAccount::tableName() . '.created_at', $this->start, $this->end])
            ->andFilterWhere(['ilike', 'openud_id', $this->openud_id]);

        return $dataProvider;
    }
}
