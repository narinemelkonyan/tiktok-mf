<?php

namespace common\models;

use backend\services\ActionManager;
use backend\services\QueueManager;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $action_id
 * @property int $status
 * @property int $start_at
 * @property int $execute_at
 * @property int|null $completed_at
 *
 * @property Action $action
 */
class Task extends ActiveRecord
{
    const STATUS_WAITING = 1;
    const STATUS_COMPLETED = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%task}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action_id', 'status', 'start_at',], 'required'],
            [['action_id', 'status', 'start_at', 'execute_at', 'completed_at'], 'default', 'value' => null],
            [['action_id', 'status'], 'integer'],
            [['start_at', 'execute_at', 'completed_at'], 'string', 'max' => 255],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::class, 'targetAttribute' => ['action_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'action_id' => Yii::t('app', 'Action ID'),
            'status' => Yii::t('app', 'Статус'),
            'start_at' => Yii::t('app', 'Дата выполнения'),
            'execute_at' => Yii::t('app', 'Execute At'),
            'completed_at' => Yii::t('app', 'Completed At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::class, ['id' => 'action_id']);
    }

    /**
     * @return array
     */
    public static function getStatus()
    {
        return [
            self::STATUS_WAITING => Yii::t('app', 'Ожидает'),
            self::STATUS_COMPLETED => Yii::t('app', 'Выполнено'),

        ];
    }

    public function composite()
    {
        $actions = QueueManager::getActions();

        if ($actions) {

            foreach ($actions as $key => $action) {
                if ($action['type_id'] == Setting::TYPE_COMMENT && $action['repeat_count'] >= 1) {
                    for ($i = 0; $i <= $action['repeat_count']; $i++) {
                        $this->queue($action);
                    }
                } else {
                    $this->queue($action);
                }

            }

        }
    }


    public function queue($action)
    {
        if ($action) {

            $startDate = QueueManager::getStartDate();
            $startAt = QueueManager::getStartAt($startDate);
            $isInSchedule = QueueManager::isInSchedule($action['from_completed'], $action['to_completed'], $startAt);
            $isInLimit = QueueManager::isInLimit($action['type_id'], $action['tiktok_account_id'], $action['max_like'], $action['max_view'], $action['max_follower']);

            if ($isInSchedule && $isInLimit) {
                $this->addTask($action['id'], $startAt);
            }
        }
    }


    public function addTask($actionId, $startAt)
    {
        $task = new Task();
        $task->action_id = $actionId;
        $task->status = Task::STATUS_WAITING;
        $task->start_at = $startAt;
        $task->save();
        return $task;
    }

    public static function check($task)
    {
        if ($task->uuid) {
            $sentData = [
                "uuid" => $task->uuid,
            ];

            $responseData = ActionManager::send($sentData, ActionManager::CHECK_URL);
            if ($responseData && isset($responseData['status']) && $responseData['status'] == ActionManager::BOT_STATUS_COMPLETED) {
                ActionManager::changeStatus($task, $responseData['status']);
                ActionManager::changeLastAuth($task->action->tiktokAccount->id);
            }

        }

    }

}
