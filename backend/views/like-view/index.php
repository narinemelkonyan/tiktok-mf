<?php

use yii\helpers\Html;
use yii\grid\GridView;
use  \common\components\grid\ActionColumn;
use \backend\forms\action\LikeForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Лайк / Просмотр');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="like-view-form-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Создать действия'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'id' => 'like-table',
            'class' => 'table table-striped table-bordered'
        ],
        'columns' => [
            'id',
            ['attribute' => 'tiktokAccountNickname',
                'label' => Yii::t('app', 'Аккаунт исполнителя'),
                'value' => function ($model) {
                    return $model->tiktokAccount->nickname;
                }
            ],
            [
                'label' => Yii::t('app', 'Дата действия'),
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->startDate,
                    'attribute' => 'startDate',
                    'pluginOptions' => [
                        'orientation' => 'bottom'
                    ]
                ]),
                'value' => function ($model) {
                    return $model->tasks ? date("Y-m-d H:i", strtotime($model->tasks[0]->start_at)) : null;
                }
            ],
            [
                'attribute' => 'type_id',
                'label' => Yii::t('app', 'Тип'),
                'filter' => LikeForm::getType(),
                'value' => function ($model) {
                    return LikeForm::getType()[$model->type_id];
                }
            ],
            'video_url:url',
            [
                'class' => ActionColumn::class,
                'contentOptions' => ['class' => 'action-column'],
                'visibleButtons' => [
                    'update' => function($model){
                        return $model->tasks ? false : true;
                    },
                    'delete' => function($model){
                        return $model->tasks ? false : true;
                    },
                ],
            ],
        ],
    ]); ?>


</div>
