<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\TiktokAccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiktok-account-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nickname') ?>

    <?= $form->field($model, 'url') ?>

    <?= $form->field($model, 'secure_id') ?>

    <?= $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'device_id') ?>

    <?php // echo $form->field($model, 'fp') ?>

    <?php // echo $form->field($model, 'iid') ?>

    <?php // echo $form->field($model, 'openud_id') ?>

    <?php // echo $form->field($model, 'max_repost') ?>

    <?php // echo $form->field($model, 'max_like') ?>

    <?php // echo $form->field($model, 'max_follower') ?>

    <?php // echo $form->field($model, 'to_completed') ?>

    <?php // echo $form->field($model, 'from_completed') ?>

    <?php // echo $form->field($model, 'registered_data') ?>

    <?php // echo $form->field($model, 'last_login') ?>

    <?php // echo $form->field($model, 'tiktok_status') ?>

    <?php // echo $form->field($model, 'group_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
