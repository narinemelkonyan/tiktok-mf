<?php

use yii\db\Migration;

/**
 * Class m200116_115409_alter_proxi_table
 */
class m200116_115409_alter_proxi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%proxi}}', 'last_used_date', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%proxi}}', 'last_used_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200116_115409_alter_proxi_table cannot be reverted.\n";

        return false;
    }
    */
}
