<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\LikeForm */

$this->title = Yii::t('app', 'Создание действий');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Like View Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="like-view-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
