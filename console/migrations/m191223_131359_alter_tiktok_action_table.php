<?php

use yii\db\Migration;

/**
 * Class m191223_131359_alter_tiktok_action_table
 */
class m191223_131359_alter_tiktok_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


        $this->addColumn('{{%action}}', 're_post_type_id', $this->integer()->null());
        $this->addColumn('{{%action}}', 're_post_count', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%action}}', 're_post_type_id');
        $this->dropColumn('{{%action}}', 're_post_count');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191223_131359_alter_tiktok_action_table cannot be reverted.\n";

        return false;
    }
    */
}
