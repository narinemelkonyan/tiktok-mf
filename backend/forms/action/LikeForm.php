<?php


namespace backend\forms\action;


use backend\services\ActionManager;
use common\models\Action;
use common\models\Setting;
use common\models\TiktokAccount;
use Yii;

class LikeForm extends Action
{

    public $groupId;

    public function rules()
    {
        return [
            [['tiktok_account_id', 'groupId', 'video_url'], 'isAccountCheck'],
            [['type_id', 'created_at', 'video_url'], 'required'],
            [['tiktok_account_id', 'created_at'], 'default', 'value' => null],
            [['tiktok_account_id', 'created_at'], 'integer'],
            [['video_url', 'text'], 'string', 'max' => 255],
            [['video_url'], 'url'],
            ['video_url', 'common\validators\VideoValidator'],
            [['tiktok_account_id'], 'exist', 'skipOnError' => true, 'targetClass' => TiktokAccount::class, 'targetAttribute' => ['tiktok_account_id' => 'id']],
        ];
    }


    public function isAccountCheck()
    {
        if ($this->tiktok_account_id == '' && $this->groupId == '') {
            $this->addError('tiktok_account_id', $this->getAttributeLabel(''));
            $this->addError('groupId', $this->getAttributeLabel('Необходимо выбрать аккаунт или группу'));
        }
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->created_at = time();
        }

        return parent::beforeValidate();
    }

    public static function getType()
    {
        return [
            Setting::TYPE_LIKE => Yii::t('app', 'Лайк'),
            Setting::TYPE_VIEW => Yii::t('app', 'Просмотр'),
        ];
    }

    public static function addSubscribeInBot($task)
    {
        if ($task) {

            $sentData = [
                "from" => $task->action->tiktokAccount->uuid,
                "point" => $task->action->video_id,
            ];

            if ($task->action->type_id == Setting::TYPE_VIEW) {
                $url = ActionManager::VIEW_URL;
            } elseif ($task->action->type_id == Setting::TYPE_LIKE) {
                $url = ActionManager::LIKE_URL;
            }

            $responseData = ActionManager::send($sentData, $url);
            if ($responseData && isset($responseData['uuid'])) {
                ActionManager::addUuid($task, $responseData['uuid']);
            }
        }
    }

    public function saveActions()
    {
        if ($this->type_id) {

            foreach ($this->type_id as $type) {

                if ($this->groupId) {

                    $accounts = TiktokAccount::find()->where(['group_id' => $this->groupId])->all();
                    if ($accounts) {
                        foreach ($accounts as $account) {
                            $this->add($account->id, $this->video_url, $this->video_id, $type);
                        }
                    }

                } elseif ($this->tiktok_account_id) {

                    $this->add($this->tiktok_account_id, $this->video_url, $this->video_id, $type);

                }
            }
        }
    }

    protected function add($account_id, $video_url, $video_id, $type_id)
    {
        $action = new Action();
        $action->created_at = time();
        $action->type_id = $type_id;
        $action->video_url = $video_url;
        $action->video_id = $video_id;
        $action->tiktok_account_id = $account_id;

        if ($action->validate()) {
            $action->save();
        }

    }
}