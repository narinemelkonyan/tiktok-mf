<?php

use yii\helpers\Html;
use yii\grid\GridView;
use  \common\components\grid\ActionColumn;
use backend\forms\import\CsvImportForm;
use \common\models\Proxy;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProxySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Прокси');
$this->params['breadcrumbs'][] = $this->title;
$import = new CsvImportForm();
?>
<div class="Proxy-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row">
        <div class="col-md-2">
            <p>
                <?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success btn-Proxy']) ?>
            </p>
        </div>
        <div class="col-md-6">
            <?= $this->render('_import', ['model' => $import]); ?>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'label' => Yii::t('app', 'Адрес '),
                'value' => function ($model) {
                    return $model->login . '@' . $model->IP . '.' . $model->port;
                }
            ],
            'check_date',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return  $model->status ? Proxy::getStatus()[$model->status] : null;
                }
            ],
            [
                'class' => ActionColumn::class,
                'contentOptions' => ['class' => 'action-column'],
                'visibleButtons' => [
                    'delete' => function($model){
                        return $model->status == Proxy::STATUS_ACTIVE ? false : true;
                    },
                ],
            ],
        ],

    ]); ?>

</div>
