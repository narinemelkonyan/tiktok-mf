<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('app','Авторизация');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="blankpage-form-field">
    <div class="page-logo m-0 w-100 align-items-center justify-content-center rounded border-bottom-left-radius-0 border-bottom-right-radius-0 px-4">
        <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
            <img src="/build/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo">
            <span class="page-logo-text mr-1"><?= Yii::t('app','TikTok MassFollowing'); ?></span>
        </a>
    </div>
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <div class="card p-4 border-top-left-radius-0 border-top-right-radius-0">
        <form action="intel_analytics_dashboard.html">
            <div class="form-group">
                <label class="form-label" for="username"><?= $model->getAttributeLabel('username'); ?></label>
                <?= $form->field($model, 'username')->textInput(['class' => 'form-control'])->label(false) ?>

            </div>
            <div class="form-group">
                <label class="form-label" for="password"><?= $model->getAttributeLabel('password'); ?></label>
                <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control'])->label(false) ?>
            </div>

            <?= Html::submitButton(Yii::t('app','Войти'), ['class' => 'btn btn-default float-right', 'name' => 'login-button']) ?>
        </form>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<video poster="/build/img/backgrounds/clouds.png" id="bgvid" playsinline autoplay muted loop>
    <source src="/build/media/video/cc.webm" type="video/webm">
    <source src="/build/media/video/cc.mp4" type="video/mp4">






