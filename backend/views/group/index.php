<?php

use yii\helpers\Html;
use yii\grid\GridView;
use  \common\components\grid\ActionColumn;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Группы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Создание новой группы'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'accounts',
                'label' => Yii::t('app','Аккаунты '),
                'value' => function ($model) {
                    $accounts = $model->getTiktokAccounts()->select(['nickname'])->asArray()->column();
                    return implode(', ', $accounts);
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'dd.MM.Y'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->created_at,
                    'attribute' => 'created_at',
                    'pluginOptions' => [
                        'orientation' => 'bottom'
                    ]
                ]),
            ],
            [
                'class' => ActionColumn::class,
                'contentOptions' => ['class' => 'action-column'],
            ],
        ],
    ]); ?>


</div>
