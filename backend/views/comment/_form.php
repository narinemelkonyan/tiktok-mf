<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\TiktokAccount;
use common\models\Group;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\CommentForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tiktok_account_id')->widget(Select2::class, [
        'data' => TiktokAccount::getAccounts(),
        'options' => ['prompt' => Yii::t('app', 'Выберите аккаунт'), 'multiple' => false],
        'pluginOptions' => [
            'tags' => false,
            'allowClear' => true,
            'tokenSeparators' => [',', ' '],
        ],
    ])->label();
    ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'groupId')->widget(Select2::class, [
            'data' =>  Group::getGroups(),
            'options' => ['prompt' => Yii::t('app', 'Выберите аккаунт'), 'multiple' => false],
            'pluginOptions' => [
                'tags' => false,
                'allowClear' => true,
                'tokenSeparators' => [',', ' '],
            ],
        ])->label(Yii::t('app','Или выберите группу '));
        ?>
    <?php endif; ?>

    <?= $form->field($model, 'video_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'repeat_count')->textInput(['type'=>'number']) ?>


    <?= $form->field($model, 'text')->textarea(['maxlength' => true])->label($model->isNewRecord ? 'Текст комментариев (Каждый комментарий в отдельной строке)' : 'Текст комментария') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
