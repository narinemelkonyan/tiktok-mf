<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\bootstrap4\BootstrapAsset;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
        'build/css/app.css',
    ];
    public $js = [
        'build/js/manifest.js',
        'build/js/vendor.js',
        'build/js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        BootstrapAsset::class,
    ];
}
