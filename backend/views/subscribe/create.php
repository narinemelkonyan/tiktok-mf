<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\forms\action\SubscribeForm */

$this->title = Yii::t('app', 'Создание действия подписки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subscribe Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
